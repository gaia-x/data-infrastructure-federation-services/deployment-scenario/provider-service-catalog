import json
import uuid
from typing import Any

from catalogue.configuration import CONTEXT_CATALOG, TYPE_CATALOG
from catalogue.domain.catalogue import Catalogue


def catalogue_to_json(catalogue: Catalogue) -> str:
    """
    The catalogue_to_json function takes a catalogue and returns a JSON representation of it.

    Args:
        catalogue: Catalogue: Specify the type of the parameter

    Returns:
        A json representation of the catalogue

    """
    if catalogue is None:
        raise ValueError("Catalogue cannot be None")

    if not isinstance(catalogue, Catalogue):
        raise TypeError("Input must be of type Catalogue")

    to_dict = catalogue.to_dict() | CONTEXT_CATALOG | TYPE_CATALOG
    return json.dumps(to_dict)


def generate_verifiable_presentation(verifiable_credentials: list[dict[str, Any]]) -> dict[str, Any]:
    """
    The generate_verifiable_presentation function takes a verifiable credential and returns a verifiable presentation.

    Args:
        verifiable_credential: Any: Pass in the verifiable credential that you want to use for your presentation

    Returns:
        A verifiable presentation
    """
    for credential in verifiable_credentials:
        if not isinstance(credential, dict):
            raise TypeError("Verifiable credential must be a dictionary.")

        if "@context" not in credential or "@id" not in credential or "@type" not in credential:
            raise ValueError("Verifiable credential must contain '@context', '@id', and '@type' properties.")

    return {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        "@id": f"urn:uuid:{uuid.uuid4()}",
        "@type": ["VerifiablePresentation"],
        "verifiableCredential": verifiable_credentials,
    }
