# Demo description

How the federated-catalog was used for the Gaia-X November 2022 Summit.

## Global view

The federated-catalog is a component provided by the abc-federation. Its role is to provide an index to search by filter
the different services provided by the provider of the abc-federation.

TODO: add schema of the global view

## Data generation of the federated-catalog

### Datasource

Several providers share with us description of their services through CSV files. These files provide us :

- services description
- datacenter location by provider
- available services by location
- certification by services by location
- CAB description to simulate CAB certification

### Components used to generate data

- user-agent will represent participants. It stores private key and expose did.json and will expose VC according to the
  participant role
  - provider user agent : store and expose SD and VC of the provider catalog
  - federation user agent : store and expose VC of ComplianceReference signed by the federation
  - CAB user agent : store and expose VC of #TODO
- VC issuer : will generate VC from Self Description
- Compliance / Labelling : #TODO (not use for the catalog generation from my thought)

## How to access endpoints ?

A single catalogue service is run per federation.

### Demo / dev environments

For the **demo** environment: <https://federated-catalogue-api.abc-federation.gaia-x.community/api/doc/>

For the **dev** environment: <https://federated-catalogue-api.abc-federation.dev.gaiax.ovh/api/doc/>

### Local

For any other non public deployment on Kubernetes, you need to create a port forwarding from catalog service:

```bash
kubectl port-forward service/federated-catalogue-api 5000:http
```

And you can access to different endpoints

- Healthcheck : http://localhost:5000/healthcheck
- Swagger : http://localhost:5000/api/doc

### API key

`x-api-key` header is compulsory to write (push/delete) data.

On the kubernetes clusters, the value of this is populated with a secret named `federation-service-catalogue-api-key`.
For write operation on the API, you must retrieve the secret value:

```sh
kubectl -n abc-federation get secret/federation-service-catalogue-api-key -o=jsonpath='{.data.api_key_authorized} | base64 -d
```

Notes on the first time deployment:
This secret is managed by [Sealed secrets](https://github.com/bitnami-labs/sealed-secrets).
The encrypted value of this secret is in the [deployment files](./deployment/packages/deployment-k8s.yaml), inside
the `SealedSecret/federation-service-catalogue-api-key` resource.

To change this secret value, install kubeseal cli on your machine and run:

```sh
# Create a standard secret.yaml file
#
#
#apiVersion: v1
#kind: Secret
#metadata:
#  name: federation-service-catalogue-api-key
#  namespace: abc-federation
#type: Opaque
#data:
#  api_key_authorized: XXXXXXX

kubeseal --scope cluster-wide -o yaml <secret.yaml >sealed-secret.yaml
```

Then put the content of `sealed-secret.yaml` in the [deployment files](./deployment/packages/deployment-k8s.yaml).

**Remember to not commit the unecrypted `secret.yaml` file!**
