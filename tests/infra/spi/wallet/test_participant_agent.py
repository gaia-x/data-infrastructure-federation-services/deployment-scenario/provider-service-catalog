import httpx
import pytest
import respx

from catalogue.infra.spi.wallet.participant_agent import (
    PARTICIPANT_AGENT_VC_REQUEST_URL,
    NotAuthorizedException,
    ParticipantAgentClient,
)


class TestParticipantAgentClient:
    @respx.mock
    async def test_generate_vc_from_catalog_should_raise_not_authorized_exception_when_calling_without_api_key(
        self, empty_catalogue
    ):
        endpoint = respx.put(url=PARTICIPANT_AGENT_VC_REQUEST_URL).respond(
            status_code=httpx.codes.FORBIDDEN, json={"message": "a valid api key is missing"}
        )

        client = ParticipantAgentClient()
        async with pytest.raises(NotAuthorizedException):
            response = await client.generate_vp_from_catalog(catalog=empty_catalogue)
            assert endpoint.called
            assert response.status_code == httpx.codes.FORBIDDEN
            assert response.json is not None
            assert response.json == {"message": "a valid api key is missing"}

    @respx.mock
    async def test_generate_vc_from_catalog_should_raise_not_authorized_exception_when_calling_without_valid_api_key(
        self, empty_catalogue
    ):
        endpoint = respx.put(url=PARTICIPANT_AGENT_VC_REQUEST_URL).respond(
            status_code=httpx.codes.FORBIDDEN, json={"message": "api key is invalid"}
        )

        client = ParticipantAgentClient()
        async with pytest.raises(NotAuthorizedException):
            response = await client.generate_vp_from_catalog(catalog=empty_catalogue)
            assert endpoint.called
            assert response.status_code == httpx.codes.FORBIDDEN
            assert response.json is not None
            assert response.json == {"message": "api key is invalid"}

    @respx.mock
    async def test_generate_vc_from_catalog_should_raise_return_vc_when_call_with_api_key_and_catalogue(
        self, empty_catalogue, vc_with_empty_catalogue
    ):
        endpoint = respx.put(url=PARTICIPANT_AGENT_VC_REQUEST_URL).respond(
            status_code=httpx.codes.OK, json=vc_with_empty_catalogue
        )

        client = ParticipantAgentClient()
        response = await client.generate_vp_from_catalog(catalog=empty_catalogue)
        assert endpoint.called
        assert response is not None
        assert response.status_code == httpx.codes.OK
