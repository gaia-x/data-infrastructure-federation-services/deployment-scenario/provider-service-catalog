import pytest

from catalogue.infra.spi.wallet.dto import (
    catalogue_to_json,
    generate_verifiable_presentation,
)


@pytest.mark.parametrize(
    "catalog_name, expected",
    [
        (
            "empty_catalogue",
            '{"gx:providedBy": '
            '"https://dufourstorage.provider.gaia-x.community/legal-participant-json/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json", '
            '"gx:getVerifiableCredentialsIDs": [], "@context": ["https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"], "@type": "gx:Catalogue"}',
        ),
        (
            "non_empty_catalogue",
            '{"gx:providedBy": '
            '"https://dufourstorage.provider.gaia-x.community/legal-participant-json/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json", '
            '"gx:getVerifiableCredentialsIDs": '
            '["did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/0987654321/data.json"], '
            '"@context": ["https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"], "@type": "gx:Catalogue"}',
        ),
    ],
)
def test_catalog_to_json(catalog_name, expected, request):
    catalog = request.getfixturevalue(catalog_name)

    assert catalogue_to_json(catalog) == expected
