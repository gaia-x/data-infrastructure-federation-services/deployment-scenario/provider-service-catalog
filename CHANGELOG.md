## [1.2.3](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/compare/1.2.2...1.2.3) (2024-04-26)


### Bug Fixes

* enable query for did:web and https prefix ([82ec766](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/commit/82ec7665f2dc3282995b97fce626bf1338888bfd))

## [1.2.2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/compare/1.2.1...1.2.2) (2024-04-19)


### Bug Fixes

* use public ECR docker registry instead of docker hub ([f98ff36](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/commit/f98ff368ecfb68269582746951151ddc0646a0cb))
* wrong id for catalogue ([cb1c4f7](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/commit/cb1c4f741038f757f058d935f766c231624b85d9))

## [1.2.1](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/compare/1.2.0...1.2.1) (2024-2-29)


### Bug Fixes

* use public ecr registry instead of docker hub" ([6a1a7bb](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/commit/6a1a7bb17c0e0b8cd908de07ddd585562422aad7))

# [1.2.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/compare/1.1.0...1.2.0) (2024-2-29)


### Features

* Add info on root api and add catalogue endpoint ([5c54579](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/commit/5c545795d913bec928ec6a62a9618b0580060d25))
