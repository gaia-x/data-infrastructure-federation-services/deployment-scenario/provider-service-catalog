import http
from typing import Generator
from unittest.mock import AsyncMock

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from catalogue.configuration import Settings, get_settings
from catalogue.domain.catalogue import Catalogue
from catalogue.infra.api.rest.routes.catalogue import create_catalogue_router

settings: Settings = get_settings()


@pytest.fixture
async def client(catalogue_service: AsyncMock) -> Generator:
    application = FastAPI()
    application.include_router(router=create_catalogue_router(catalogue_service=catalogue_service), tags=["Catalog"])
    async with AsyncClient(app=application, base_url="http://testserver") as client:
        yield client


class TestCatalogueBlueprint:
    @pytest.mark.anyio
    async def test_get_catalog_should_return_empty_catalog_when_no_vc(
        self, catalogue_service: AsyncMock, client: AsyncClient, empty_catalogue: Catalogue
    ) -> None:
        catalogue_service.get.is_async = True
        catalogue_service.get.return_value = empty_catalogue

        response = await client.get("/catalog")

        assert response.json() == {
            "gx:providedBy": settings.did_url,
            "gx:getVerifiableCredentialsIDs": [],
            "@type": "gx:Catalogue",
            "@context": ['https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#'],
        }
        assert response.status_code == http.HTTPStatus.OK

    @pytest.mark.anyio
    async def test_get_catalog_should_return_non_empty_catalog_when_vc_indexed(
        self, catalogue_service: AsyncMock, client: AsyncClient, non_empty_catalogue: Catalogue, vc_did: str
    ) -> None:
        catalogue_service.get.is_async = True
        catalogue_service.get.return_value = non_empty_catalogue

        response = await client.get("/catalog")

        assert response.json() == {
            "gx:providedBy": settings.did_url,
            "@type": "gx:Catalogue",
            "gx:getVerifiableCredentialsIDs": [vc_did],
            "@context": ['https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#'],
        }
        assert response.status_code == http.HTTPStatus.OK

    @pytest.mark.anyio
    async def test_delete_catalog_should_return_403_when_no_api_key_provided(self, client: AsyncClient) -> None:
        response = await client.delete("/catalog")
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "Not authenticated" in response.text

    @pytest.mark.anyio
    async def test_delete_catalog_should_return_403_when_invalid_api_key_provided(self, client: AsyncClient) -> None:
        response = await client.delete("/catalog", headers={"x-api-key": "invalid_api_key"})
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "api key is invalid" in response.text

    @pytest.mark.anyio
    async def test_delete_catalog_should_return_204_when_api_key_provided(
        self, catalogue_service: AsyncMock, client: AsyncClient
    ) -> None:
        catalogue_service.delete_catalog.is_async = True
        catalogue_service.delete_catalog.return_value = None

        response = await client.delete("/catalog", headers={"x-api-key": settings.api_key_authorized})
        assert response.status_code == http.HTTPStatus.NO_CONTENT

    @pytest.mark.anyio
    async def test_index_item_in_catalog_should_return_403_when_no_api_key_provided(
        self, catalogue_service: AsyncMock, client: AsyncClient
    ) -> None:
        response = await client.post("/catalog/items/")
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "Not authenticated" in response.text

    @pytest.mark.anyio
    async def test_index_item_in_catalog_should_return_403_when_invalid_api_key_provided(
        self, catalogue_service: AsyncMock, client: AsyncClient
    ) -> None:
        response = await client.post("/catalog/items/", headers={"x-api-key": "invalid_api_key"})
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "api key is invalid" in response.text

    @pytest.mark.anyio
    async def test_index_item_in_catalog_should_return_201_when_api_key_provided(
        self, catalogue_service: AsyncMock, client: AsyncClient, vc_did: str
    ) -> None:
        catalogue_service.index_vc_from_did.is_async = True
        catalogue_service.index_vc_from_did.return_value = None

        response = await client.post("/catalog/items/", json=vc_did, headers={"x-api-key": settings.api_key_authorized})

        assert response.status_code == http.HTTPStatus.CREATED
        assert catalogue_service.index_vc_from_did.call_count == 1

    @pytest.mark.anyio
    async def test_delete_item_from_catalog_should_return_403_when_no_api_key_provided(
        self, client: AsyncClient
    ) -> None:
        response = await client.delete("/catalog/items/")
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "Not authenticated" in response.text

    @pytest.mark.anyio
    async def test_delete_item_from_catalog_should_return_403_when_invalid_api_key_provided(
        self, client: AsyncClient
    ) -> None:
        response = await client.delete("/catalog/items/", headers={"x-api-key": "invalid_api_key"})
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "api key is invalid" in response.text

    @pytest.mark.anyio
    async def test_delete_item_from_catalog_should_return_201_when_api_key_provided(
        self, catalogue_service: AsyncMock, client: AsyncClient, vc_did: str
    ) -> None:
        catalogue_service.delete_vc_from_did.is_async = True
        catalogue_service.delete_vc_from_did.return_value = None

        response = await client.request(
            "DELETE",
            "/catalog/items/",
            json=vc_did,
            headers={"x-api-key": settings.api_key_authorized},
        )

        assert response.status_code == http.HTTPStatus.NO_CONTENT
