import asyncio
import os
from typing import Any
from unittest.mock import AsyncMock

import pytest
from redis.asyncio.client import Redis

from catalogue.configuration import Settings, get_settings
from catalogue.domain.catalogue import Catalogue
from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.domain.interfaces.repository import CatalogueRepository
from catalogue.domain.interfaces.wallet import WalletClient
from catalogue.services.catalogue import CatalogueService
from catalogue.services.healthcheck import HealthcheckService

os.environ["TESTING"] = "TRUE"

settings: Settings = get_settings()
VC_DID = "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/0987654321/data.json"


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"


@pytest.fixture
def vc_did() -> str:
    yield VC_DID


@pytest.fixture
def empty_catalogue() -> Catalogue:
    yield Catalogue(provided_by=settings.did_url)


@pytest.fixture
def vc_with_empty_catalogue() -> Any:
    yield {
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://schemas.abc-federation.gaia-x.community/wip/contexts/for-credentials.json",
        ],
        "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/6ddcfaafd60e609ab5ff975cfa84d848aca51e3cb1c52dd25e0e9b95441aa59c/data.json",
        "@type": ["VerifiableCredential", "Catalogue"],
        "credentialSubject": {
            "@context": {"@vocab": "gx:Catalogue/", "gx": "https://w3id.org/gaia-x/", "schema": "https://schema.org/"},
            "@type": "Catalogue",
            "providedBy": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        },
        "issuanceDate": "2023-06-12T10:04:18.354907+00:00",
        "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "proof": {
            "created": "2023-06-12T10:04:18.354907+00:00",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..WmQ6Wh89afIG2n4hjfTPBk6X9VkvFLH2iFLF0PL1xreJgjkHQOm85G_drWgy-95YUNuQrsaLD06qUcbdytaRbkQk41dvLQqgoUuahWmLtC5F6IGv8OkK86mkjY8BvFz9u8X1cdwZGExNS8-1lVDf608FU2dY1M-ONTmEy5JGCZeYGUFpbOcnSz_kmrg10TayXBlczCfGZVMP4WxZAOJJNNJqroETdgu3XeTXsBGaJALKKtPpOY_2-aMZX6tk1WMXDTiP44WdFB7q0BQSvnxLLtpNX1QN-OxvUg3PymRiuG68ggY3oCqFjUX4IQgFs25WEewtP9rL85OrGG2vrrj7sg",
            "proofPurpose": "assertionMethod",
            "type": "JsonWebSignature2020",
            "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        },
    }


@pytest.fixture
def vp_with_empty_catalogue(vc_with_empty_catalogue) -> Any:
    yield {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        "@id": "urn:uuid:ce64b087-75bb-4ff4-bb99-6d516a4ec0c4",
        "@type": ["VerifiablePresentation"],
        "verifiableCredential": [vc_with_empty_catalogue],
    }


@pytest.fixture
def non_empty_catalogue(vc_did: str) -> Catalogue:
    yield Catalogue(provided_by=settings.did_url, get_verifiable_credentials_ids=[vc_did])


@pytest.fixture
def vc_with_non_empty_catalogue(vc_did: str) -> Any:
    yield {
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
            "https://schemas.abc-federation.gaia-x.community/wip/contexts/for-credentials.json",
        ],
        "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/6ddcfaafd60e609ab5ff975cfa84d848aca51e3cb1c52dd25e0e9b95441aa59c/data.json",
        "@type": ["VerifiableCredential", "Catalogue"],
        "credentialSubject": {
            "@context": {"@vocab": "gx:Catalogue/", "gx": "https://w3id.org/gaia-x/", "schema": "https://schema.org/"},
            "@type": "Catalogue",
            "providedBy": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
            "getVerifiableCredentialsIDs": [vc_did],
        },
        "issuanceDate": "2023-06-12T10:04:18.354907+00:00",
        "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "proof": {
            "created": "2023-06-12T10:04:18.354907+00:00",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..WmQ6Wh89afIG2n4hjfTPBk6X9VkvFLH2iFLF0PL1xreJgjkHQOm85G_drWgy-95YUNuQrsaLD06qUcbdytaRbkQk41dvLQqgoUuahWmLtC5F6IGv8OkK86mkjY8BvFz9u8X1cdwZGExNS8-1lVDf608FU2dY1M-ONTmEy5JGCZeYGUFpbOcnSz_kmrg10TayXBlczCfGZVMP4WxZAOJJNNJqroETdgu3XeTXsBGaJALKKtPpOY_2-aMZX6tk1WMXDTiP44WdFB7q0BQSvnxLLtpNX1QN-OxvUg3PymRiuG68ggY3oCqFjUX4IQgFs25WEewtP9rL85OrGG2vrrj7sg",
            "proofPurpose": "assertionMethod",
            "type": "JsonWebSignature2020",
            "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        },
    }


@pytest.fixture
def vp_with_empty_catalogue(vc_with_empty_catalogue: Any) -> Any:
    yield {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        "@id": "urn:uuid:ce64b087-75bb-4ff4-bb99-6d516a4ec0c4",
        "@type": ["VerifiablePresentation"],
        "verifiableCredential": [vc_with_empty_catalogue],
    }


@pytest.fixture
def catalogue_repository() -> AsyncMock:
    yield AsyncMock(spec=CatalogueRepository)


@pytest.fixture
def wallet_client() -> AsyncMock:
    yield AsyncMock(spec=WalletClient)


@pytest.fixture
def redis_client() -> AsyncMock:
    yield AsyncMock(spec=Redis)


@pytest.fixture
def message_publisher() -> AsyncMock:
    yield AsyncMock(spec=MessagePublisher)


@pytest.fixture
def catalogue_service() -> AsyncMock:
    yield AsyncMock(spec=CatalogueService)


@pytest.fixture()
def healthcheck_service() -> AsyncMock:
    yield AsyncMock(spec=HealthcheckService)
