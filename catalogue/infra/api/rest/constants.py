API_TITLE = "Provider Catalogue"
API_DESCRIPTION = """
REST API of the GXFS-FR Provider Catalogue

[Source repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue)
"""

TAGS_METADATA = [
    {"name": "Catalogue", "description": "How to retrieve provider's catalogue."},
    {"name": "Common", "description": "Common routes (Healthcheck, test api key)"},
    {"name": "Indexation", "description": "Manage provider's catalogue indexation."},
    {"name": "Utilities", "description": "Utility methods"},
]
