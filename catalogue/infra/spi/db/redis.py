from typing import Any

from redis.asyncio.client import Redis
from redis.exceptions import RedisError

from catalogue.domain.interfaces.repository import CatalogueRepository


class RedisCatalogueRepository(CatalogueRepository):
    def __init__(self, url: str = "redis://127.0.0.1:6379/0") -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the Redis client and connects to a Redis server.

        Args:
            self: Represent the instance of the object itself
            url: str: Specify the redis server to connect to

        Returns:
            None
        """
        self.client = Redis.from_url(url=url, decode_responses=True)

    async def keys(self, prefix: str) -> list[str]:
        """
        The keys function returns a list of keys that match the given prefix.

        Args:
            self: Represent the instance of the class
            prefix: str: Specify the prefix of the keys that we want to get

        Returns:
            A list of keys in the database that match the prefix
        """
        return await self.client.keys(prefix)

    async def store(self, did: str, document: str):
        """
        The store function takes a document id and the document itself as input.
        It then stores the document in Redis using the given id as key.

        Args:
            self: Represent the instance of the class
            did: str: Store the document id
            document: str: Store the document in the database

        Returns:
            The document that was stored
        """
        await self.client.set(name=did, value=document)

    async def delete(self, did: str) -> str:
        """
        The delete function deletes a document from the database.


        Args:
            self: Represent the instance of the class
            did: str: Specify the key of the value to be deleted

        Returns:
            The value of the key that was deleted
        """

        return await self.client.getdel(name=did)

    async def delete_list(self, dids: list) -> Any:
        """
        The delete function deletes a document from the database.


        Args:
            self: Represent the instance of the class
            dids: list: Specify the keys of the value to be deleted

        Returns:
            The value of the key that was deleted
        """
        if not isinstance(dids, list):
            raise TypeError("dids must be a list")

        if dids:
            return await self.client.delete(*dids)

        return 0

    async def delete_all(self):
        """
        The delete_all function deletes all keys in the Redis database.


        Args:
            self: Represent the instance of the class

        Returns:
            The number of keys that were removed
        """
        await self.client.flushall()

    async def ping(self) -> bool:
        """
        The ping function is used to test the connection between the client and server.
        It returns a boolean value of True if it succeeds, or False if it fails.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value indicating whether the connection is still alive
        """
        try:
            await self.client.ping()
        except RedisError:
            return False
        return True
