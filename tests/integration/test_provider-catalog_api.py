from typing import Dict

import pytest
from fastapi.testclient import TestClient


@pytest.mark.integration
# pylint: disable=no-member
class TestApp:
    """Test main Flask app module"""

    @pytest.mark.usefixtures("client")
    def test_healh_check(self, client: TestClient):
        """Test /healthcheck endpoint (static)"""
        response = client.get("/healthcheck")
        assert response.status_code == 200

        assert "application/json" in response.headers["content-type"]
        response_json = response.json()
        assert "catalogue repository (Redis)" in response_json

    @pytest.mark.usefixtures("client")
    def test_get_catalog(self, client: TestClient):
        """Test /healthcheck endpoint (static)"""
        response = client.get("/catalog")
        assert response.status_code == 200

        assert "application/ld+json" in response.headers["content-type"]

        response_json: Dict[str, str] = response.json()
        assert "@context" in response_json
