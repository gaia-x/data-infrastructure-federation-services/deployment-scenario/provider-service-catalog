# Catalog Synchronisation

## Context

Federated Catalog is feeding with two ways :
- API expose an endpoint to push data inside the federated-catalog (Push mode)
- Federated Catalog pull the participant-catalog to synchronize its graph federatedCatalogueDatabase (Pull mode)

---
**Note**: Federated Catalog API expose an endpoint to add json-ld object to the catalog. The graph will store the
object and create association if IRI of the node are identical to existing node.
---

## Global view architecture

![Federated Catalogue global view](./images/federated_catalog_overview.png)

---
**Note**: An API Key protects Federated Catalogue API for demo purpose. When provider catalogue pushes updated into
federated catalogue, it must authenticate with this API Key.
When identification and authentication service will be implemented, it will replace this mechanism by a proper method.
---
## Terminology

| Term                            | Definition                                                                                                                                 | Gitlab repository                                                                                                                                                          |
|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Wallet                          | An entity used by the Holder to receive, store, present, and manage Verifiable Credentials and key material.                               |
| User Agent                      | User Agent is an implementation of Wallet for demo purpose                                                                                 | [User Agent repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent)                                                  |
| VC Issuer                       | VC Issuer is an internal service that generates VC for wallet                                                                              | [VC Issuer repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer)                                                    |
| Provider service catalogue API  | API to share provider service catalog with a federation                                                                                    |                                                                                                                                                                            |
| Federated service catalogue     | Service offering catalogue that belongs to a Federation. It is an aggregation of catalogues offered by providers belonging to a federation | [Common Federated Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue) |
| Federated catalogue Web App     | Front-end to search into federated catalogue                                                                                               | [Federated catalogue Web App repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2)                             |
| Federated service catalogue API | API to search in Federated service offering catalogue                                                                                      | [Common Federated Service Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue) |



## Use case and flow

### Create a new service-offering

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Compliance
    participant Labelling
    participant Federated Catalog

    Bob->>Bob: Create service offering SD
    Bob->>+Wallet: Store service offering SD
    Wallet-->>-Bob: OK

    Bob->>+VC Issuer: Ask VC Service-Offering generation with Provider as Issuer
    VC Issuer-->>-Bob: VC Service-Offering generated

    Bob->>+Wallet: Store VC Service-Offering
    Wallet-->>-Bob: OK

    Bob->>+Compliance: Ask Compliance for Service-Offering
    Compliance-->>-Bob: VC Compliance

    Bob->>+Labelling: Ask Labelling for Service-Offering
    Labelling-->>-Bob: VC Labelling

    Bob->>+Wallet: Store VC Labelling and VC Compliance
    Wallet-->>-Bob: OK

    opt bob wants to index his service
    Bob->>+Provider Catalog: Index VCs to make them available
    Provider Catalog-->>-Bob: OK
    end

    opt bob does'nt want to wait the synchronisation
    Bob->>+Wallet: Ask VP for Service & Compliance & Labelling
    Wallet-->>-Bob: VP generated

    Bob->>+Federated Catalog: Add VP inside federated-catalog to make it searchable
    Federated Catalog-->>-Bob: OK (service is searchable by federated catalog)
    end
```

### New Provider create his catalog

Federated catalog don't know the provider catalog so provider needs to register by pushing its catalog object.

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Compliance
    participant Labelling
    participant Federated Catalog

    Bob->>Bob: Create service offering SD
    Bob->>+Wallet: Store service offering SD
    Wallet-->>-Bob: OK

    Bob->>+VC Issuer: Ask VC generation with Provider as Issuer
    VC Issuer-->>-Bob: VC generated

    Bob->>+Wallet: Store service offering VC
    Wallet-->>-Bob: OK

    Bob->>+Compliance: Ask Compliance for Service-Offering
    Compliance-->>-Bob: VC Compliance

    Bob->>+Labelling: Ask Labelling for Service-Offering
    Labelling-->>-Bob: VC Labelling

    Bob->>+Wallet: Store VC Labelling and VC Compliance
    Wallet-->>-Bob: OK

    Bob->>Bob: Create Catalog SD with VC did he wants to share

    Bob->>+VC Issuer: Ask VC generation with Provider as Issuer
    VC Issuer-->>-Bob: Catalog VC generated

    Bob->>+Wallet: Ask VP with Catalog inside
    Wallet-->>-Bob: VP Catalog

    Bob->>+Federated Catalog: VP Catalog to register catalog inside federation
    Federated Catalog-->>-Bob: OK
```


### Existing Provider update his catalog

Federated catalog know the provider catalog so federated catalog will pull provider catalog to synchronize objects.

```mermaid
sequenceDiagram
    actor Bob as [Provider]<br>Bob
    participant Wallet as [Wallet]<br>Provider Wallet
    participant VC Issuer
    participant Provider Catalog
    participant Compliance
    participant Federated Catalog

    Bob->>Bob: Regenerate his catalog

    Bob->>Bob: Create Catalog SD with VC did he wants to share

    Bob->>+VC Issuer: Ask VC generation with Provider as Issuer
    VC Issuer-->>-Bob: Catalog VC generated

    Bob->>+Wallet: Post VP with Catalog inside
    Wallet-->>-Bob: VP Catalog

    Bob->>+Provider Catalog: Post VP with Catalog inside
    Provider Catalog-->>-Bob: VP Catalog

    loop Daily
        Federated Catalog->>+Provider Catalog: Ask last version of catalog
        Provider Catalog-->>-Federated Catalog: Catalog last version
        Federated Catalog->>Federated Catalog: Resynchronize Catalog
    end
```
