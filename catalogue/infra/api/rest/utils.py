import hmac
import http
import logging

from fastapi import HTTPException, Security
from fastapi.security import APIKeyHeader

from catalogue.configuration import get_settings

DEFAULT_SECURITY_HEADER = Security(APIKeyHeader(name="x-api-key", auto_error=True))

log = logging.getLogger(__name__)


async def api_key_auth(api_key_header: str = DEFAULT_SECURITY_HEADER):
    """
    The api_key_auth function is a custom authentication function that takes the api key from the request header and
    checks if it matches with our API_KEY_AUTHORIZED. If it does, then we return the api key.
    Otherwise, we raise an HTTPException with status code 403 (Forbidden) and detail message
    &quot;api key is invalid&quot;.

    Args:
        api_key_header: str: Get the value of the header with key api_key_header

    Returns:
        The api_key_header value if the api key is valid

    """
    settings = get_settings()
    if not hmac.compare_digest(api_key_header, settings.api_key_authorized):
        raise HTTPException(status_code=http.HTTPStatus.FORBIDDEN, detail="api key is invalid")

    return api_key_header
