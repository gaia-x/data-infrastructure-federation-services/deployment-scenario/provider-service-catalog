import json

import pytest
from redis.exceptions import RedisError

from catalogue.infra.spi.message_queue.redis import RedisMessagePublisher


class TestRedisPublisher:
    @pytest.mark.anyio
    async def test_publish_message_successfully(self, mocker):

        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.publish.return_value = 1
        redis_client_from_url_mock = mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)
        redis_publisher = RedisMessagePublisher(channel_name="test_channel")

        message = {"msg": "test_message"}

        await redis_publisher.publish_catalogue(message)

        redis_client_from_url_mock.assert_called_once_with("redis://127.0.0.1:6379/0", decode_responses=True)
        redis_client_mock.publish.assert_called_once_with("test_channel", message=json.dumps(message))

    @pytest.mark.anyio
    async def test_publish_message_not_string(self, mocker):
        redis_publisher = RedisMessagePublisher(channel_name="test_channel")
        message = 123
        with pytest.raises(ValueError):
            await redis_publisher.publish_catalogue(message)

    @pytest.mark.anyio
    async def test_publish_message_redis_error(self, mocker):
        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.publish.side_effect = RedisError("Redis error")
        redis_client_from_url_mock = mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        redis_publisher = RedisMessagePublisher(channel_name="test_channel")

        message = {"msg": "test_message"}
        with pytest.raises(RedisError):
            await redis_publisher.publish_catalogue(message)

        redis_client_from_url_mock.assert_called_once_with("redis://127.0.0.1:6379/0", decode_responses=True)
        redis_client_mock.publish.assert_called_once_with("test_channel", message=json.dumps(message))

    @pytest.mark.anyio
    async def test_message_not_published_empty_string(self, mocker):
        redis_client_mock = mocker.AsyncMock()
        redis_client_from_url_mock = mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        redis_publisher = RedisMessagePublisher(channel_name="test_channel")
        empty_message = ""

        with pytest.raises(ValueError):
            await redis_publisher.publish_catalogue(empty_message)

        redis_client_from_url_mock.assert_called_once_with("redis://127.0.0.1:6379/0", decode_responses=True)
        redis_client_mock.publish.assert_not_called()

    @pytest.mark.anyio
    async def test_message_not_published_none(self, mocker):
        redis_client_mock = mocker.AsyncMock()
        redis_client_from_url_mock = mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        redis_publisher = RedisMessagePublisher(channel_name="test_channel")
        none_message = None

        with pytest.raises(ValueError):
            await redis_publisher.publish_catalogue(none_message)

        redis_client_from_url_mock.assert_called_once_with("redis://127.0.0.1:6379/0", decode_responses=True)
        redis_client_mock.publish.assert_not_called()

    def test_redis_client_instantiated_correctly(self, mocker):
        redis_client_from_url_mock = mocker.patch("redis.asyncio.client.Redis.from_url")
        RedisMessagePublisher(channel_name="test_channel", url="redis://localhost:6379/1")
        redis_client_from_url_mock.assert_called_once_with("redis://localhost:6379/1", decode_responses=True)

    def test_channel_name_stored_correctly(self, mocker):
        redis_publisher = RedisMessagePublisher(channel_name="test_channel")
        assert redis_publisher.channel_name == "test_channel"

    @pytest.mark.anyio
    async def test_ping_returns_true_if_connection_is_alive(self, mocker):
        # Setup
        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.ping.return_value = True
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        redis_publisher = RedisMessagePublisher(channel_name="test_channel")

        # Exercise
        result = await redis_publisher.ping()

        # Verify
        assert result is True
        redis_client_mock.ping.assert_called_once()

    @pytest.mark.anyio
    async def test_ping_returns_false_if_connection_is_lost(self, mocker):
        # Setup
        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.ping.side_effect = RedisError()
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        redis_publisher = RedisMessagePublisher(channel_name="test_channel")

        # Exercise
        result = await redis_publisher.ping()

        # Verify
        assert result is False
        redis_client_mock.ping.assert_called_once()
