# Federated and Provider Catalog Documentation

The catalog is able to stock Service Offering Self Descriptions with other information associated and to search services
via API.

GraphDB which is a graph database is used to store all json-ld files received on federated catalogue.

## Glossary

| Term                | Description                                                                                                                    |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Participant-Catalog | Each participant can expose through this component the list of Verifiable Credentials he wants to share with the federation.   |
| Federated-Catalog   | Concatenation of the participant catalogs inside a graph database with feature of search and filtering Verifiable Credentials. |

## Federated Catalog feed

Each federation's participant pushes their catalog into federated catalog thanks to a pub/sub broker.
When Federated catalog receives a message in this broker, it will check some criteria before updating its database.

[To know more about catalog synchronisation.](./catalog-synchronisation.md)

## Federated Catalog query

TODO

Sparql not frontally exposed

## How to launch local Federated Catalog

### Launch an GraphDB instance

You can use this [graphdb docker image](https://hub.docker.com/r/khaller/graphdb-free/tags) to launch your instance.

```bash
docker run --rm -p 127.0.0.1:7200:7200 --name graphdb-instance-name -t khaller/graphdb-free:10.1.0
```

### Launch a Redis instance

```bash
docker run --rm -p 127.0.0.1:6379:6379 --name some-redis -d redis:7.2-rc-alpine
```

### Create venv python and launch application

```bash
python3.10 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
cd code
REDIS_HOST=127.0.0.1 API_PORT_EXPOSED=5002 GRAPHDB_URL=http://localhost:7200 API_KEY_AUTHORIZED=my_api_key python main.py
```

When the application is started, a graphdb repository named "abc-federation" is created inside the database.

### Feed Federated Catalog with example data

You have an example of IKOULA provider catalog [here](./json-ld-example/ikoula-dataset.json).

You can load data via API with this command :

```
curl -X POST --header 'x-api-key: my_api_key' -H "Content-Type: application/json" \
-d @./documentation/json-ld-example/ikoula-dataset.json http://localhost:5002/json-ld
```

And you can check data with this query without filter :

```
curl --location --request POST 'http://localhost:5002/query' \
--header 'Content-Type: application/json' --data-raw '{}'
```
