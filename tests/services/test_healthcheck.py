import pytest

from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.domain.interfaces.repository import CatalogueRepository
from catalogue.services.healthcheck import HealthcheckService


class TestHealthcheckService:
    @pytest.mark.anyio
    async def test_ping_returns_dict_with_single_key_value_pair(self, mocker):
        mock_repository = mocker.AsyncMock(spec=CatalogueRepository)
        mock_repository.ping.return_value = True

        mock_publisher = mocker.AsyncMock(spec=MessagePublisher)
        mock_publisher.ping.return_value = False

        healthcheck_service = HealthcheckService(mock_repository, mock_publisher)

        result = await healthcheck_service.ping()
        assert len(result) == 2

    @pytest.mark.anyio
    async def test_ping_returns_boolean_value(self, mocker):
        mock_repository = mocker.AsyncMock(spec=CatalogueRepository)
        mock_repository.ping.return_value = True

        mock_publisher = mocker.AsyncMock(spec=MessagePublisher)
        mock_publisher.ping.return_value = False

        healthcheck_service = HealthcheckService(mock_repository, mock_publisher)

        result = await healthcheck_service.ping()
        assert all(isinstance(value, bool) for value in result.values())

    @pytest.mark.anyio
    async def test_ping_returns_false_when_repository_is_down(self, mocker):
        mock_repository = mocker.AsyncMock(spec=CatalogueRepository)
        mock_repository.ping.return_value = False

        mock_publisher = mocker.AsyncMock(spec=MessagePublisher)
        mock_publisher.ping.return_value = False

        healthcheck_service = HealthcheckService(mock_repository, mock_publisher)
        result = await healthcheck_service.ping()

        assert list(result.values())[0] == False
        assert list(result.values())[1] == False

    @pytest.mark.anyio
    async def test_ping_returns_false_when_publisher_is_down(self, mocker):
        mock_repository = mocker.AsyncMock(spec=CatalogueRepository)
        mock_repository.ping.return_value = True

        mock_publisher = mocker.AsyncMock(spec=MessagePublisher)
        mock_publisher.ping.return_value = False

        healthcheck_service = HealthcheckService(mock_repository, mock_publisher)
        result = await healthcheck_service.ping()

        assert list(result.values())[0] == True
        assert list(result.values())[1] == False

    @pytest.mark.anyio
    async def test_ping_returns_false_when_both_repository_and_publisher_are_down(self, mocker):
        mock_repository = mocker.AsyncMock(spec=CatalogueRepository)
        mock_repository.ping.return_value = False

        mock_publisher = mocker.AsyncMock(spec=MessagePublisher)
        mock_publisher.ping.return_value = False

        healthcheck_service = HealthcheckService(mock_repository, mock_publisher)
        result = await healthcheck_service.ping()

        assert not all(list(result.values()))
