from abc import ABCMeta, abstractmethod


class CatalogueRepository(metaclass=ABCMeta):
    @abstractmethod
    async def keys(self, prefix: str) -> list[str]:
        """
        The keys function returns a list of keys that match the given prefix.

        Args:
            self: Refer to the instance of the class
            prefix: str: Specify the prefix of the keys to be returned

        Returns:
            A list of all the keys in the trie that start with prefix

        """
        raise NotImplementedError

    @abstractmethod
    async def store(self, did: str, document: str) -> None:
        """
        The store function is used to store a document in the database.

        Args:
            self: Represent the instance of the class
            did: str: Identify the document
            document: str: Store the document

        Returns:
            Nothing
        """
        raise NotImplementedError

    @abstractmethod
    async def delete(self, did: str) -> str:
        """
        The delete function deletes a document from the database.


        Args:
            self: Represent the instance of the class
            did: str: Specify the id of the document to be deleted

        Returns:
            A string
        """
        raise NotImplementedError

    @abstractmethod
    async def delete_list(self, dids: list) -> str:
        """
        The delete function deletes a list of document from the database.


        Args:
            self: Represent the instance of the class
            dids: list: Specify the ids of the document to be deleted

        Returns:
            Any
        """
        raise NotImplementedError

    @abstractmethod
    async def delete_all(self) -> None:
        """
        The delete_all function deletes all the rows in the table.

        Args:
            self: Refer to the object itself

        Returns:
            None
        """
        raise NotImplementedError

    @abstractmethod
    async def ping(self) -> bool:
        """
        The ping function is used to check if the server is up and running.
        It returns a boolean value, True if the server responds with a pong message, False otherwise.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value of whether the connection is alive or not
        """
        raise NotImplementedError
