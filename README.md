# Provider Catalogue

## Table of Contents
- [Description](#description)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Environment Variables](#environment-variables)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)

## Description

Provider Catalogue is a comprehensive management tool for service offerings, data products, and other resources
associated with a provider. It facilitates the addition or removal of self-description Verifiable Credentials (VCs)
related to a provider's offerings. Furthermore, it provides the capability to push the provider's catalogue
to a federated catalogue or to the Gaia-X Centralized Event Service, offering greater visibility and accessibility
for the provider's services.

This project is developed using Python 3.12.x



## Prerequisites

This project has several prerequisites that you need to meet before you can install and run it on your machine.
Here are the prerequisites:

1. **Python**: The project is primarily developed in Python. Make sure you have Python installed on your machine.
The project requires Python version 3.12 or higher.

2. **Poetry**: This project uses Poetry for dependency management. If you don't have Poetry installed,
you can install it following the instructions on the [official Poetry website](https://python-poetry.org/docs/#installation).
The project requires Poetry version 1.8 or higher.

3. **Docker**: Docker is used for creating and managing the project's containers. Make sure Docker is installed and
running on your machine. You can download Docker from the
[official Docker website](https://www.docker.com/products/docker-desktop).

4. **Docker Compose**: Docker Compose is used to define and run multi-container Docker applications.

5. **Environment Variables**: You will need to set up several environment variables for the project to run correctly.
You can set these variables in a `.env` file in the root of your project.

Please ensure that all these prerequisites are met before you proceed with the installation and configuration of the project.

## Installation

Before you can run the Provider Catalogue, you need to install the necessary dependencies. This project uses Python,
so you will need to have it installed on your machine. Here are the steps to install the project:

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue.git
```

2. Navigate to the project directory:

```bash
cd provider-catalogue
```

3. Install the project dependencies. This project uses Poetry for dependency management:

```bash
poetry install
```

## Usage

> **Important Notice**: Before pushing the catalogue into the federated catalogue or the Gaia-X Centralized Event Service (CES),
> it is crucial to generate a Verifiable Presentation. This process involves calling the participant-agent and vc-issuer components.
> The participant-agent is responsible for managing the participant's identity and credentials, while the vc-issuer component sign Verifiable Credentials (VCs).
> These VCs are then bundled into a Verifiable Presentation, which serves as a proof of the provider's identity and offerings.
> Failure to generate a Verifiable Presentation before pushing the catalogue may result in errors or the provider's offerings not being correctly listed in the federated catalogue or CES.
> Please ensure that you have can connect to participant-agent and vc-issuer components before pushing the catalogue.

You can run Provider Catalogue in a Docker container or locally on your machine. Here are the instructions for both methods.

### Setting environment variables

The easiest way to set environment variables is to create a `.env` file in the root of your project and add the environment variables there:

```bash
touch .env
```

Then open the `.env` file and add your environment variables:

```env
LOG_LEVEL=DEBUG
ENVIRONMENT=DEV
API_KEY_AUTHORIZED=your_api_key
API_PORT_EXPOSED=5001
REDIS_HOST=redis://redis:6379/0
REDIS_PUB_SUB_HOST=redis://redis:6379/1
CES_URL=https://ces-development.lab.gaia-x.eu
FEDERATION_REPOSITORY=abc-federation
PARTICIPANT_NAME=your_participant_name
PARENT_DOMAIN=your_domain
PARTICIPANT_AGENT_HOST=your_agent_host
PARTICIPANT_AGENT_SECRET_KEY=your_secret_key
```

Replace `your_api_key`, `your_participant_name`, `your_domain`, `your_agent_host`, and `your_secret_key` with your actual values.

> Note: See [Environment Variables](#environment-variables) to have a description of each environment variable.

### Running the application locally

First of all, you need to run a redis server on your machine. You can do this by running the following command:

```bash
redis-server
```
> Note: Make sure you have redis installed on your machine. If not, you can install it by following the instructions on the [official Redis website](https://redis.io/download).

Then, you can run the application locally by running the following command:
```bash
Run the application:

```bash
poetry run python -m catalogue
```
This will start the application. It will be accessible on `http://localhost:5001`.

To stop the application, press `Ctrl + C`.

### Running the application in a Docker container
The docker-compose file is already provided in the project. You can run the application in a Docker container by running the following command:

```bash
docker-compose up
```

This will start the application. It will be accessible on `http://localhost:5001`.

To stop the application, press `Ctrl + C`.

## Environment Variables
Here is the list of environment variables used in your project, along with their explanations and default values:

| Environment Variable           | Explanation                                                                                                                                          | Default Values                          |
|--------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------|
| `LOG_LEVEL`                    | Sets the application's log level. Possible values include `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `CRITICAL`.                                       | `DEBUG`                                 |
| `ENVIRONMENT`                  | Sets the application's environment. Possible values could be `DEV`, `TEST`, `STAGING`, or `PROD`.                                                    | `DEV`                                   |
| `API_KEY_AUTHORIZED`           | This is the API Key for authorization. The value would be a string that you generate and keep secret.                                                | None (required)                         |
| `API_PORT_EXPOSED`             | This is the exposed port for the API. The value would be an integer representing the port number, such as `5001`.                                    | `5001`                                  |
| `REDIS_HOST`                   | This is the Redis Host DSN. The value would be a string in the format `redis://hostname:port/db_number`, such as `redis://127.0.0.1:6379/0`.         | `redis://127.0.0.1:6379/0`              |
| `REDIS_PUB_SUB_HOST`           | This is the Redis Pub/Sub Host DSN. The value would be a string in the format `redis://hostname:port/db_number`, such as `redis://127.0.0.1:6379/1`  | `redis://127.0.0.1:6379/1`              |
| `CES_URL`                      | This is the URL for the centralized event system. The value would be a string representing the URL, such as `https://ces-development.lab.gaia-x.eu`. | `https://ces-development.lab.gaia-x.eu` |
| `FEDERATION_REPOSITORY`        | This is the federation repository. The value would be a string representing the name of the federation repository, such as abc-federation.           | `abc-federation`                        |
| `PARTICIPANT_NAME`             | This is the name of the participant. The value would be a string representing the participant's name, such as `ovhcloud`.                            | None (required)                         |
| `PARENT_DOMAIN`                | This is the application domain name. The value would be a string representing the domain name, such as `provider.dev.gaiax.ovh`.                     | None (required)                         |
| `PARTICIPANT_AGENT_HOST`       | This is the URL for the participant agent. The value would be a string representing the URL, such as `https://ovhcloud.provider.dev.gaiax.ovh`.      | None (required)                         |
| `PARTICIPANT_AGENT_SECRET_KEY` | API key for the participant agent to access to participant agent API.                                                                                | None (required)                         |

## Deployment Instructions on Kubernetes
This project includes a Kubernetes deployment files that you can use to deploy the application on a Kubernetes cluster.

> Note: You can find the base deployment files in the `deployment/packages/base` directory. This project uses Kustomize for managing Kubernetes resources and kubectl for deploying them.

To deploy the application on a Kubernetes cluster, follow these steps:

1. **Create a Namespace**: You need to create a namespace where you will deploy your application, you can do this using the `kubectl` command:

```bash
kubectl create namespace ovhcloud
```

2. **Create a Secret for API_KEY_AUTHORIZED **: You need to create a Kubernetes Secret to store API_KEY_AUTHORIZED environment variables. You can do this using the `kubectl` command:

```bash
kubectl create secret generic catalogue-api-key --from-literal=api_key_authorized=a_secret -n ovhcloud
```

This command creates a Secret named `catalogue-api-key` in the `ovhcloud` namespace.

3. **Create a ConfigMap for Environment Variables**: You need to create a ConfigMap to store the environment variables for the application. You can do this using the `kubectl` command:

```bash
kubectl create configmap catalog-configuration --from-env-file=.env -n ovhcloud
```

4. **Deploy the Application**: You can deploy the application using the `kubectl` command:

```bash
kubectl apply -k deployment/packages/base/ -n ovhcloud
```

This command deploys the application to the `ovhcloud` namespace using the configuration files in the `deployment/packages/base/` directory.

4. **Verify the Deployment**: You can verify that the application has been deployed successfully using the `kubectl` command:

```bash
kubectl get pods -n ovhcloud
```

This command lists all the pods in the `ovhcloud` namespace. You should see a pod for your application in the list.

5. **Access the Application**: If your application exposes a web interface, you can access it using port forwarding. You can do this using the `kubectl` command:

```bash
kubectl port-forward svc/catalogue-api 5001:80 -n ovhcloud
```

This command forwards traffic from port 5001 on your local machine to port 80 on the `catalogue-api` service in the `provider-catalogue` namespace. You can access the application by navigating to `http://localhost:5001` in your web browser.

Please note that these instructions are for a basic deployment. Depending on your specific requirements, you may need to modify these instructions. For example, you may need to use a different namespace, or you may need to configure additional resources such as Ingresses or Persistent Volumes.

## Contributing

To contribute to Provider Catalogue, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

For more information on how to contribute to this project, please refer to the [CONTRIBUTING.md](./CONTRIBUTING.md) file.

## License

This project uses the following license: [Apache v2.0 License](./LICENSE).
