import pytest
from pydantic_core import Url

from catalogue.configuration import (
    EnvironmentEnum,
    LogLevelEnum,
    Settings,
    get_settings,
)

settings: Settings = get_settings()


class TestConfig:
    """Test class for the Config module."""

    def test_default_values(self, monkeypatch):
        monkeypatch.delenv("LOG_LEVEL", raising=False)
        monkeypatch.delenv("ENVIRONMENT", raising=False)
        monkeypatch.delenv("API_KEY_AUTHORIZED", raising=False)
        monkeypatch.delenv("REDIS_HOST", raising=False)
        monkeypatch.delenv("REDIS_PUB_SUB_HOST", raising=False)
        monkeypatch.delenv("FEDERATION_REPOSITORY", raising=False)
        monkeypatch.delenv("PARTICIPANT_NAME", raising=False)
        monkeypatch.delenv("PARENT_DOMAIN", raising=False)
        monkeypatch.delenv("PARTICIPANT_AGENT_HOST", raising=False)
        monkeypatch.delenv("PARTICIPANT_AGENT_SECRET_KEY", raising=False)

        with pytest.raises(ValueError):
            Settings()
            assert settings.log_level == LogLevelEnum.debug
            assert settings.environment == EnvironmentEnum.dev
            assert settings.api_port_exposed == 5001
            assert settings.redis_host == "redis://127.0.0.1:6379/0"
            assert settings.redis_pub_sub_host == "redis://127.0.0.1:6379/1"
            assert settings.federation_repository == "abc-federation"
            assert settings.participant_name is None
            assert settings.parent_domain is None
            assert settings.participant_agent_host is None

    def test_set_and_retrieve_values(self):
        settings = Settings(
            log_level=LogLevelEnum.warning,
            environment=EnvironmentEnum.test,
            api_key_authorized="my_secret_key",
            api_port_exposed=8080,
            redis_host="redis://127.0.0.1:6379/2",
            redis_pub_sub_host="redis://127.0.0.1:6379/3",
            federation_repository="xyz-federation",
            participant_name="my_participant",
            parent_domain="my_domain.com",
            participant_agent_host="my_agent_host",
            participant_agent_secret_key="my_agent_secret_key",
        )
        assert settings.log_level == LogLevelEnum.warning
        assert settings.environment == EnvironmentEnum.test
        assert settings.api_key_authorized == "my_secret_key"
        assert settings.api_port_exposed == 8080
        assert settings.redis_host == Url("redis://127.0.0.1:6379/2")
        assert settings.redis_pub_sub_host == Url("redis://127.0.0.1:6379/3")
        assert settings.federation_repository == "xyz-federation"
        assert settings.participant_name == "my_participant"
        assert settings.parent_domain == "my_domain.com"
        assert settings.participant_agent_host == "my_agent_host"
        assert settings.participant_agent_secret_key == "my_agent_secret_key"

    def test_str_encryption(self):
        settings = Settings(api_key_authorized="my_secret_key")
        assert isinstance(settings.api_key_authorized, str)
        assert settings.api_key_authorized == "my_secret_key"
        assert isinstance(settings.model_dump()["api_key_authorized"], str)

    def test_secrets_with_carriage_return(self):
        settings = Settings(api_key_authorized="\rmy_secret_key\n")
        assert isinstance(settings.api_key_authorized, str)
        assert settings.api_key_authorized == "my_secret_key"
        assert isinstance(settings.model_dump()["api_key_authorized"], str)

    def test_missing_required_variables(self, monkeypatch):
        monkeypatch.delenv("LOG_LEVEL", raising=False)
        monkeypatch.delenv("ENVIRONMENT", raising=False)
        monkeypatch.delenv("API_KEY_AUTHORIZED", raising=False)
        monkeypatch.delenv("REDIS_HOST", raising=False)
        monkeypatch.delenv("REDIS_PUB_SUB_HOST", raising=False)
        monkeypatch.delenv("FEDERATION_REPOSITORY", raising=False)
        monkeypatch.delenv("PARTICIPANT_NAME", raising=False)
        monkeypatch.delenv("PARENT_DOMAIN", raising=False)
        monkeypatch.delenv("PARTICIPANT_AGENT_HOST", raising=False)
        monkeypatch.delenv("PARTICIPANT_AGENT_SECRET_KEY", raising=False)

        with pytest.raises(ValueError):
            Settings()

        with pytest.raises(ValueError):
            Settings(
                participant_name="my_participant", parent_domain="my_domain.com", participant_agent_host="my_agent_host"
            )

        with pytest.raises(ValueError):
            Settings(
                api_key_authorized="my_secret_key",
                participant_name="my_participant",
                parent_domain="my_domain.com",
            )

        with pytest.raises(ValueError):
            Settings(
                api_key_authorized="my_secret_key",
                participant_name="my_participant",
                participant_agent_host="my_agent_host",
            )

        with pytest.raises(ValueError):
            Settings(
                api_key_authorized="my_secret_key",
                parent_domain="my_domain.com",
                participant_agent_host="my_agent_host",
            )

    def test_invalid_log_levels(self):
        with pytest.raises(ValueError):
            Settings(log_level="invalid")

        with pytest.raises(ValueError):
            Settings(log_level=123)

    def test_invalid_environments(self):
        with pytest.raises(ValueError):
            Settings(environment="invalid")

        with pytest.raises(ValueError):
            Settings(environment=123)

    def test_federation_repository(self):
        """Ensure federation repository is not modified."""
        assert settings.federation_repository == "abc-federation"

    def test_api_key_authorized(self):
        """Ensure API key is correctly loaded from env file."""
        assert settings.api_key_authorized == "this-is-a-secure-key"

    def test_api_port_exposed(self):
        """Ensure API port is correctly loaded from env file."""
        assert settings.api_port_exposed == 5001

    def test_redis_host_exposed(self):
        """Ensure Redis Host is correctly loaded from env file."""
        assert settings.redis_host == Url("redis://127.0.0.1:6379/0")

    def test_redis_pubsub_host_exposed(self):
        """Ensure Redis Host is correctly loaded from env file."""
        assert settings.redis_pub_sub_host == Url("redis://127.0.0.1:6379/1")

    def test_participant_name(self):
        """Ensure Participant name is correctly loaded from env file."""
        assert settings.participant_name == "dufourstorage"

    def test_participant_agent_exposed(self):
        """Ensure participant agent Host is correctly loaded from env file."""
        assert settings.participant_agent_host == "http://127.0.0.1:5002"

    def test_participant_agent_secret_key(self):
        """Ensure participant agent secret key is correctly loaded from env file."""
        assert settings.participant_agent_secret_key == "secret-key"
