import logging
import ssl
from typing import Final

import httpx

from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.infra.spi.message_queue.adapters.factory import to_cloud_event

log = logging.getLogger(__name__)


class BadRequestException(Exception):
    """
    The BadRequestException class is a custom exception class that is raised when a HTTP request returns a status code
    of 400 (Bad Request) or 409 (Conflict). It inherits from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class CESException(Exception):
    """
    The BadRequestException class is a custom exception class that is raised when a HTTP request returns a status code
    of 500 (Internal Server Error). It inherits from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class CESMessagePublisher(MessagePublisher):
    _DEFAULT_TIMEOUT: Final = 30.0

    def __init__(self, url: str) -> None:
        log.debug(f"Creating publisher to CES => {url}")
        self.ces_url = url

    async def publish_catalogue(self, message: dict):
        return

    async def ping(self) -> bool:
        return True

    async def publish_vc(self, message: dict):
        log.debug(f"publishing to ces : {self.ces_url}")
        if not isinstance(message, dict):
            raise ValueError("Message must be a dict")

        log.debug(f"message : {message['id']}")
        _, payload = to_cloud_event(payload=message)

        log.debug(f"payload : {payload.decode('utf-8')}")

        client = httpx.AsyncClient()
        try:
            api_url = f"{self.ces_url}/credentials-events"
            response = await client.post(
                url=api_url,
                timeout=self._DEFAULT_TIMEOUT,
                content=payload,
                headers={"Content-Type": "application/json"},
            )

            match response.status_code:
                case httpx.codes.OK | httpx.codes.CREATED:
                    return
                case httpx.codes.BAD_REQUEST:
                    raise BadRequestException(str(response.content))
                case httpx.codes.CONFLICT:
                    raise BadRequestException(str(response.content))
                case _:
                    raise CESException(str(response.content))
        except ssl.SSLCertVerificationError:
            log.error(f"{self.ces_url} is not reachable: SSL Certificate issue")
            raise
        except httpx.ConnectError:
            log.error(f"{self.ces_url} is not reachable: SSL Certificate issue")
            raise
        except Exception:
            log.error(f"{self.ces_url} is not reachable: SSL Certificate issue")
            raise
        finally:
            await client.aclose()
