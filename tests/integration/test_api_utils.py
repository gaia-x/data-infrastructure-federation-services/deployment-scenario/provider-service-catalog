import json

import pytest
from httpx import Client

from catalogue.configuration import Settings, get_settings

settings: Settings = get_settings()


class TestAPIUtils:
    """Test class for the api_key_required module"""

    @pytest.mark.usefixtures("client")
    def test_api_key_is_required(self, client: Client):
        """Ensure api key is required on specific endpoints"""
        secured_endpoints = {
            "/test-api-key": "get",
        }
        for endpoint, method in secured_endpoints.items():
            match method:
                case "get":
                    response = client.get(endpoint)
                case "delete":
                    response = client.delete(endpoint)
                case "post":
                    response = client.post(endpoint)

            assert response.status_code == 403
            assert "Not authenticated" in response.json()["detail"]

    @pytest.mark.usefixtures("client")
    def test_api_key_invalid(self, client):
        """Ensure invalid api key returns 403"""
        response = client.get("/test-api-key", headers={"x-api-key": "dummy"})
        assert response.status_code == 403
        assert "api key is invalid" in response.json()["detail"]

    @pytest.mark.usefixtures("client")
    def test_api_key_valid(self, client):
        """Ensure api key return 200"""
        response = client.get("/test-api-key", headers={"x-api-key": settings.api_key_authorized.get_secret_value()})
        assert response.status_code == 200
        assert "Coucou tout le monde" in response.text
