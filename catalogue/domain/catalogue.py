from dataclasses import dataclass, field
from typing import Optional

from dataclasses_json import config, dataclass_json


@dataclass_json
@dataclass
class Catalogue:
    provided_by: str = field(metadata=config(field_name="gx:providedBy"))
    get_verifiable_credentials_ids: Optional[str | list[str]] = field(
        metadata=config(field_name="gx:getVerifiableCredentialsIDs"), default_factory=list
    )
