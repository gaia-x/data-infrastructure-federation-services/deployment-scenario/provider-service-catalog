import asyncio

import pytest
from redis.exceptions import RedisError

from catalogue.infra.spi.db.redis import RedisCatalogueRepository


class TestRedisCatalogueRepository:
    @pytest.mark.anyio
    async def test_keys_returns_matching_keys(self, mocker):
        # Setup
        prefix = "test:"
        keys = ["test:1", "test:2", "test:3"]

        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.keys.return_value = keys
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        result = await repo.keys(prefix)

        # Verify
        redis_client_mock.keys.assert_called_once_with(prefix)
        assert result == keys

    @pytest.mark.anyio
    async def test_keys_returns_empty_list_if_no_keys_match_prefix(self, mocker):
        # Setup
        prefix = "test:"
        keys = []

        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.keys.return_value = keys
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        result = await repo.keys(prefix)

        # Verify
        redis_client_mock.keys.assert_called_once_with(prefix)
        assert result == keys

    @pytest.mark.anyio
    async def test_store_stores_document_in_redis(self, mocker):
        # Setup
        did = "1"
        document = "test document"

        redis_client_mock = mocker.AsyncMock()
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        await repo.store(did, document)

        # Verify
        redis_client_mock.set.assert_called_once_with(name=did, value=document)

    @pytest.mark.anyio
    async def test_delete_deletes_document_from_database(self, mocker):
        # Setup
        did = "1"
        document = "test document"

        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.getdel.return_value = document
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        result = await repo.delete(did)

        # Verify
        redis_client_mock.getdel.assert_called_once_with(name=did)
        assert result == document

    @pytest.mark.anyio
    async def test_delete_all_deletes_all_keys_in_redis_database(self, mocker):
        # Setup
        redis_client_mock = mocker.AsyncMock()
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        await repo.delete_all()

        # Verify
        redis_client_mock.flushall.assert_called_once()

    @pytest.mark.anyio
    async def test_ping_returns_true_if_connection_is_alive(self, mocker):
        # Setup
        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.ping.return_value = True
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        result = await repo.ping()

        # Verify
        assert result is True
        redis_client_mock.ping.assert_called_once()

    @pytest.mark.anyio
    async def test_ping_returns_false_if_connection_is_lost(self, mocker):
        # Setup
        redis_client_mock = mocker.AsyncMock()
        redis_client_mock.ping.side_effect = RedisError()
        mocker.patch("redis.asyncio.client.Redis.from_url", return_value=redis_client_mock)

        repo = RedisCatalogueRepository()

        # Exercise
        result = await repo.ping()

        # Verify
        assert result is False
        redis_client_mock.ping.assert_called_once()
