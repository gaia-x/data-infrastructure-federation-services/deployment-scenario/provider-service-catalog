from abc import ABCMeta, abstractmethod
from typing import Any

from catalogue.domain.catalogue import Catalogue


class WalletClient(metaclass=ABCMeta):
    @abstractmethod
    async def generate_vp_from_catalog(self, catalog: Catalogue, provider_vc: dict[str, Any] | None = None) -> Any:
        """
        The generate_vp_from_catalog function is used to generate a Verifiable Presentation from the given Catalogue.

        Args:
            self: Bind the method to an object
            catalog: Catalogue: Pass in the catalog object
            provider_vc: dict[str: Pass in the did document of the provider
            Any] | None: Specify that the parameter is optional

        Returns:
            A verifiable presentation
        """
        raise NotImplementedError
