import asyncio
import json
import logging
import ssl
from typing import Any

import httpx
from httpx import AsyncClient

from catalogue.configuration import Settings, get_settings
from catalogue.domain.catalogue import Catalogue
from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.domain.interfaces.repository import CatalogueRepository
from catalogue.domain.interfaces.wallet import WalletClient
from catalogue.infra.spi.wallet.participant_agent import WrongVCRequestException
from catalogue.utilities.helpers import get_compliance_file_name, transform_did_into_url

log = logging.getLogger(__name__)
settings: Settings = get_settings()


class CatalogueService:
    def __init__(
        self, repository: CatalogueRepository, wallet_client: WalletClient, publishers: list[MessagePublisher] = None
    ) -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the object with a repository to use for data access.

        Args:
            self: Represent the instance of the class
            repository: CatalogueRepository: Pass in the repository object

        Returns:
            Nothing
        """
        self.repository = repository
        self.wallet_client = wallet_client
        self.publishers = publishers if publishers is not None else []

    async def get(self) -> Catalogue:
        """
        The get function returns a Catalogue object, which is a list of all the verifiable credentials
        that are stored in the repository. The catalogue is used by other participants to discover what
        verifiable credentials they can request from this participant.

        Args:
            self: Represent the instance of the class

        Returns:
            A catalogue object containing a list of verifiable credentials ids
        """
        return Catalogue(
            provided_by=settings.did_url,
            get_verifiable_credentials_ids=await self.repository.keys(prefix=f"*{settings.participant_name}*"),
        )

    async def delete_catalog(self):
        """
        The delete_catalog function deletes all the items in the catalog.


        Args:
            self: Represent the instance of the class

        Returns:
            Nothing
        """
        await self.repository.delete_all()

    async def _get_provider_vc(self) -> dict[str, Any] | None:
        log.debug(f"Get provider verifiable credentials from did: {settings.provider_vc_did_url}")
        provider_url = transform_did_into_url(settings.provider_vc_did_url)

        return await self._retrieve_json_content(provider_url)

    async def publish_catalog_to_federated_catalog(self) -> bool:
        """
        The publish_catalog function publishes catalog into pubsub broker


        Args:
            self: Represent the instance of the class

        Returns:
            Nothing
        """
        log.debug("Publishing catalog")

        if self.publishers is None or len(self.publishers) == 0:
            log.debug("No publisher - Nothing to do")
            return False

        log.debug("1- Getting current catalog")
        catalogue = await self.get()

        try:
            provider_vc = await self._get_provider_vc()

            verifiable_presentation = await self.wallet_client.generate_vp_from_catalog(
                catalog=catalogue, provider_vc=provider_vc
            )

            log.debug("3- Send VP message")
            if verifiable_presentation is not None:
                for publisher in self.publishers:
                    await publisher.publish_catalogue(verifiable_presentation)

            return True
        except WrongVCRequestException as e:
            log.exception(e)
            return False

    @staticmethod
    async def _retrieve_json_content(json_url: str) -> Any:
        """
        The __retrieve_json_content function is a helper function that retrieves the JSON content from the URL provided.
        It returns a Python object (dict, list, etc.) containing all the data in the JSON file.

        Args:
            json_url: str: Specify the url of the json file

        Returns:
            A json object
        """
        headers = {
            "Accept": "application/json",
        }

        client = AsyncClient()
        try:
            response = await client.get(json_url, headers=headers, timeout=60)

            match response.status_code:
                case httpx.codes.OK:
                    return response.json()
                case _:
                    return None
        except ssl.SSLCertVerificationError:
            log.error(f"{json_url} is not reachable: SSL Certificate issue")
            return None
        except httpx.ConnectError:
            log.error(f"{json_url} is not reachable: Connect error")
            return None
        finally:
            await client.aclose()

    async def index_vc_from_did(
        self, did: str, publish_to_federated_catalog: bool = True, publish_to_ces: bool = False
    ) -> Any:
        """
        The index_vc_from_did method in the CatalogueService class retrieves a JSON document from a given DID
        (Decentralized Identifier) URL, stores it in the repository, and optionally publishes it to a federated catalog
        and CES (Cloud Event Service).

        Example Usage
        ```python
        # Initialize the CatalogueService object
        repository = CatalogueRepository()
        wallet_client = WalletClient()
        publishers = [MessagePublisher()]
        service = CatalogueService(repository, wallet_client, publishers)

        # Call the index_vc_from_did method
        did = "did:web:example.com"
        json_doc = await service.index_vc_from_did(
            did, publish_to_federated_catalog=True, publish_to_ces=False
        )
        print(json_doc)
        ```

        :param did (str): The DID (Decentralized Identifier) of the JSON document to be retrieved and indexed.
        :param publish_to_federated_catalog (bool, optional): A flag indicating whether to publish the catalog to a
        federated catalog. Default is True.
        :param publish_to_ces (bool, optional): A flag indicating whether to publish the VC (Verifiable Credential) to
        a CES (Compliance Enforcement Service). Default is False.
        :return json_doc (Any): The retrieved JSON document from the given DID URL.
        """
        json_doc = await CatalogueService._retrieve_json_content(transform_did_into_url(did))

        if json_doc is not None:
            await self.repository.store(did=did, document=json.dumps(json_doc))

            if publish_to_federated_catalog:
                await self.publish_catalog_to_federated_catalog()

            if publish_to_ces:
                await self.__publish_to_ces(json_doc)

        return json_doc

    async def __publish_to_ces(self, json_doc: dict[str, Any]):
        if json_doc is None:
            return

        if "@id" not in json_doc:
            return

        if "credentialSubject" not in json_doc:
            return

        credential_subject = json_doc["credentialSubject"]
        if "type" in credential_subject and credential_subject["type"] == "gx:ServiceOffering":
            did = json_doc["@id"]

            service_offering_did = did.replace("data.json", "gaiax-compliance-service-offering.json")
            json_doc = await CatalogueService._retrieve_json_content(transform_did_into_url(service_offering_did))

            await self.publish_vc_to_ces(vc=json_doc)

    async def index_vc_from_dids(
        self, dids: list, publish_to_federated_catalogue: bool = True, publish_to_ces: bool = False
    ) -> Any:
        """
        The index_vc_from_dids method in the CatalogueService class retrieves a JSON document from given DIDs
        (Decentralized Identifier) URL, stores them in the repository, and optionally publishes them to a federated
        catalogue and CES (Cloud Event Service).

        :param dids (list): The DIDs (Decentralized Identifier) of the JSON documents to be retrieved and indexed.
        :param publish_to_federated_catalogue (bool, optional): A flag indicating whether to publish the catalogue to a
        federated catalogue. Default is True.
        :param publish_to_ces (bool, optional): A flag indicating whether to publish the VC (Verifiable Credential) to
        a CES (Compliance Enforcement Service). Default is False.
        :return json_doc (Any): The retrieved JSON documents from the given DID URL.
        """
        json_docs = []
        for did in dids:
            json_doc = await CatalogueService._retrieve_json_content(transform_did_into_url(did))
            if json_doc:
                json_docs.append({"did": did, "json": json_doc})

        for json_doc in json_docs:
            await self.repository.store(did=json_doc["did"], document=json.dumps(json_doc["json"]))

            if publish_to_ces:
                await self.__publish_to_ces(json_doc["json"])

        if publish_to_federated_catalogue:
            await self.publish_catalog_to_federated_catalog()

        return json_docs

    async def delete_vc_from_did(self, did: str) -> Any:
        """
        The delete_vc_from_did function deletes a VC from the DID repository.
            Args:
                did (str): The DID of the VC to be deleted.

        Args:
            self: Bind the method to an object
            did: str: Specify the did of the vc to be deleted

        Returns:
            The did_document
        """
        did_document = await self.repository.delete(did)

        await self.publish_catalog_to_federated_catalog()

        return did_document

    async def delete_vcs_from_dids(self, dids: list, publish_to_federated_catalogue: bool = True) -> Any:
        """
        The delete_vcs_from_dids function deletes VCs from the DID repository.
            Args:
                dids (list): The DIDs of the VCs to be deleted.

        Args:
            self: Bind the method to an object
            dids: list: Specify the did of the vc to be deleted

        Returns:
            OK or KO
            :param publish_to_federated_catalogue:
        """
        result = await self.repository.delete_list(dids)

        if publish_to_federated_catalogue:
            await self.publish_catalog_to_federated_catalog()

        return result

    async def publish_vc_to_ces(self, vc: dict[str, Any]):
        """
        The publish_vc_to_ces method in the CatalogueService class publishes a Verifiable Credential (VC) to a Cloud
        Event Service (CES). It retrieves the VC from the given vc parameter, generates a compliance file name based
        on the VC's object type, and retrieves the JSON document from the transformed DID URL. If the JSON document is
        not None, it stores the document in the repository and publishes it to the CES using the publish_vc method of
        each publisher in the publishers list.

        ### Example Usage
        ```python
        repository = CatalogueRepository()
        wallet_client = WalletClient()
        publishers = [MessagePublisher()]
        service = CatalogueService(repository, wallet_client, publishers)
        vc = {
            "type": ["VerifiableCredential", "User"],
            "id": "did:web:example.com/data.json",
        }
        await service.publish_vc_to_ces(vc)
        ```
        :param vc (dict[str, Any]): A dictionary representing the Verifiable Credential to be published to the CES.
        It should have a "type" key with a list of object types and an "id" key with the DID URL of the VC.
        :return None. The method does not return any value.

        """
        if vc is None or not isinstance(vc, dict):
            return

        object_types = vc.get("type", vc.get("@type", []))
        if len(object_types) <= 1:
            return

        vc_file_name = get_compliance_file_name(object_types[1])
        did_web = vc.get("id", vc.get("@id"))
        if did_web is not None:
            did_web = did_web.replace("data.json", vc_file_name)
            json_doc = await CatalogueService._retrieve_json_content(transform_did_into_url(did_web))

            if json_doc is not None:
                await self.repository.store(did=did_web, document=json.dumps(json_doc))

                for publisher in self.publishers:
                    await publisher.publish_vc(message=json_doc)

    async def get_ghost_items(self) -> set[str]:
        """
        Find item indexed but not accessible.

        :return:
        """
        catalogue: Catalogue = await self.get()

        tasks = []
        for did in catalogue.get_verifiable_credentials_ids:
            tasks.append(asyncio.create_task(self.try_did(did)))

        results = await asyncio.gather(*tasks)
        unresolved = set(results)
        unresolved.remove(None)

        return unresolved

    @staticmethod
    async def try_did(did: str) -> str | None:
        """
        Try to retrieve content thanks to the did.

        :param did:
        :return: None if no accessible.
        """
        if await CatalogueService._retrieve_json_content(transform_did_into_url(did)) is None:
            return did
