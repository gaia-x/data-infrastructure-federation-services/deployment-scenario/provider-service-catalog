import http
from typing import Generator
from unittest.mock import AsyncMock

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from catalogue.infra.api.rest.routes import create_common_router


@pytest.fixture
def healthcheck_value() -> bool:
    yield True


@pytest.fixture
async def client(healthcheck_service: AsyncMock, healthcheck_value: bool) -> Generator:
    healthcheck_service.ping.is_async = True
    healthcheck_service.ping.return_value = {"catalogue repository (Redis)": healthcheck_value}

    application = FastAPI()
    application.include_router(router=create_common_router(healthcheck_service=healthcheck_service), tags=["Common"])
    async with AsyncClient(app=application, base_url="http://testserver") as client:
        yield client


class TestCommonRoutes:
    @pytest.mark.anyio
    async def test_healthcheck_should_return_200(self, client: AsyncClient) -> None:
        response = await client.get("/healthcheck")
        assert response.status_code == http.HTTPStatus.OK
        assert "application/json" in response.headers.values()
        assert '{"catalogue repository (Redis)":true}' in response.text

    @pytest.mark.anyio
    @pytest.mark.parametrize("healthcheck_value", [False])
    async def test_healthcheck_should_return_503_when_no_redis_connection(
        self, healthcheck_value: bool, client: AsyncClient
    ) -> None:
        response = await client.get("/healthcheck")
        assert response.status_code == http.HTTPStatus.SERVICE_UNAVAILABLE
        assert '{"catalogue repository (Redis)":false}' in response.text

    @pytest.mark.anyio
    async def test_secured_route_should_return_403_if_api_key_is_missing(self, client: AsyncClient) -> None:
        response = await client.get("/test-api-key")
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "Not authenticated" in response.text

    @pytest.mark.anyio
    async def test_secured_route_should_return_403_if_api_key_is_invalid(self, client: AsyncClient) -> None:
        response = await client.get("/test-api-key", headers={"x-api-key": "invalid_api_key"})
        assert response.status_code == http.HTTPStatus.FORBIDDEN
        assert "api key is invalid" in response.text
