import http
import logging

from fastapi import APIRouter, Depends, Request
from fastapi.responses import JSONResponse

from catalogue.infra.api.rest.utils import api_key_auth
from catalogue.services.healthcheck import HealthcheckService

log = logging.getLogger(__name__)


def create_common_router(healthcheck_service: HealthcheckService) -> APIRouter():
    """
    The create_common_router function is used to create a router for the common routes.

    Args:
        healthcheck_service: HealthcheckService: Create a healthcheck service

    Returns:
        A router
    """
    common_router = APIRouter()

    @common_router.get("/", status_code=http.HTTPStatus.OK)
    def root_info(request: Request):
        """
        The root function is used to share usage information like doc url.
        :return:
        """

        data = {"healthcheck": f"{request.url!s}healthcheck", "url_swagger": f"{request.url!s}api/doc"}

        return JSONResponse(content=data)

    @common_router.get("/healthcheck", status_code=http.HTTPStatus.OK)
    async def healthcheck():
        """
        The healthcheck function is used to check the health of the Provider Service Catalogue.
        It returns a string indicating that it is OK.

        Args:

        Returns:
            A string
        """
        statuses = await healthcheck_service.ping()

        if not all(statuses.values()):
            return JSONResponse(content=statuses, status_code=http.HTTPStatus.SERVICE_UNAVAILABLE)

        return JSONResponse(content=statuses)

    @common_router.get("/test-api-key", dependencies=[Depends(api_key_auth)])
    async def secured_route():
        """
        The secured_route function is a secured route.

        Args:

        Returns:
            A string
        """
        return "Coucou tout le monde"

    return common_router
