# Developer Guide

## Technologies

- FastAPI to expose endpoints
- Swagger to explain endpoints and format
- Redis to have a cache between provider catalog and federated catalog
- Redis is also used as pub/sub broker

## Util commands

See the content of the [Makefile](./Makefile) to view all the util commands.

```bash
make help
Please use 'make <target>' where <target> is one of

  install       install packages and prepare environment
  clean         remove all temporary files
  lint          run the code linters
  format        reformat code
  test          run all the tests
  image         build a docker image for provider catalog
  test-ci       launch integration tests with docker-compose
  pre-commit    run pre-commit checks
  launch-python launch locally provider catalog

Check the Makefile to know exactly what each target is doing.
```

### Local dev setup

- Install poetry (v1.3.x)
- Install pre-commit
- Install Docker and Docker Compose

Execute:

```sh
make install
```
### Creating a .envrc file

Create a .envrc for local development purpose.

```bash
cp .envrc.example .envrc
```

Change value of `API_KEY_AUTHORIZED` in your .envrc file

### Pre-commit and Git

On every commit, a set of pre-commit hooks **must** be runned to ensure a minimum of quality checks. pre-commit hooks are also executed on the Gitlab-ci pipelines.

Every commit will trigger pre-commit execution.

You can manually launch pre-commit:

```sh
# pre-commit only on modified files
pre-commit run
# pre-commit on all files
pre-commit run -a
```

### Launch image in local environment

```bash
# run a redis server thanks to docker-compose
# and launch fast-api application
make launch-python

# Check connection
curl -X GET http://localhost:5001/healthcheck
```
