from unittest.mock import AsyncMock

import pytest
from fastapi.testclient import TestClient

from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.domain.interfaces.repository import CatalogueRepository
from catalogue.domain.interfaces.wallet import WalletClient
from catalogue.infra.api.rest.api import create_fastapi_application
from catalogue.infra.spi.db.redis import RedisCatalogueRepository
from catalogue.infra.spi.message_queue.redis import RedisMessagePublisher
from catalogue.services.catalogue import CatalogueService
from catalogue.services.healthcheck import HealthcheckService


@pytest.fixture
def client() -> TestClient:
    catalogue_repository: CatalogueRepository = RedisCatalogueRepository()

    publisher: MessagePublisher = AsyncMock(spec=RedisMessagePublisher)
    wallet_client: WalletClient = AsyncMock(spec=WalletClient)
    catalogue_service = CatalogueService(
        repository=catalogue_repository, wallet_client=wallet_client, publishers=publisher
    )
    healthcheck_service = HealthcheckService(repository=catalogue_repository)

    application = create_fastapi_application(
        catalogue_service=catalogue_service, healthcheck_service=healthcheck_service
    )

    yield TestClient(app=application)
