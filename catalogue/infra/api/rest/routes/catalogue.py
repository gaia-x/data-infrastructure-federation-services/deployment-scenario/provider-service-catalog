import http
import json
import logging
from typing import Annotated

import fastapi
from fastapi import APIRouter, Body, Depends
from starlette.responses import Response

from catalogue.configuration import CONTEXT_CATALOG, TYPE_CATALOG
from catalogue.domain.catalogue import Catalogue
from catalogue.infra.api.rest.utils import api_key_auth
from catalogue.services.catalogue import CatalogueService

log = logging.getLogger(__name__)


def catalog_to_json(catalog: Catalogue) -> str:
    """
    The catalog_to_json function takes a Catalogue object and returns a JSON string representation of it.

    Args:
        catalog: Catalogue: Specify the type of the parameter

    Returns:
        A json string representing the given catalog
    """
    to_dict = catalog.to_dict() | CONTEXT_CATALOG | TYPE_CATALOG
    return json.dumps(to_dict)


def create_catalogue_router(catalogue_service: CatalogueService) -> APIRouter:
    """
    The create_catalogue_router function creates a router for the Catalogue API.

    Args:
        catalogue_service: CatalogueService: Pass the catalogueservice object to the function

    Returns:
        A fastapi router, which is a special object that can be used to define
    """
    catalogue_router = APIRouter()

    @catalogue_router.get("/catalog", status_code=http.HTTPStatus.OK, tags=["Catalogue"])
    async def get_catalog() -> fastapi.Response:
        json_data = catalog_to_json(await catalogue_service.get())
        return fastapi.Response(content=json_data, media_type="application/ld+json")

    @catalogue_router.get("/catalogue", status_code=http.HTTPStatus.OK, tags=["Catalogue"])
    async def get_catalogue() -> fastapi.Response:
        json_data = catalog_to_json(await catalogue_service.get())
        return fastapi.Response(content=json_data, media_type="application/ld+json")

    @catalogue_router.delete(
        "/catalog", status_code=http.HTTPStatus.NO_CONTENT, dependencies=[Depends(api_key_auth)], tags=["Catalogue"]
    )
    async def delete_catalog() -> None:
        await catalogue_service.delete_catalog()

    @catalogue_router.delete(
        "/catalogue", status_code=http.HTTPStatus.NO_CONTENT, dependencies=[Depends(api_key_auth)], tags=["Catalogue"]
    )
    async def delete_catalogue() -> None:
        await catalogue_service.delete_catalog()

    @catalogue_router.post("/catalog/items/", dependencies=[Depends(api_key_auth)], tags=["Indexation"])
    async def post_item_into_catalog(
        did: Annotated[str, Body()], publish_to_catalog: bool = True, publish_to_ces: bool = False
    ) -> Response:
        log.debug(f"DID Received : {did}")
        log.debug(f"publish_to_catalog : {publish_to_catalog}")
        log.debug(f"publish_to_ces : {publish_to_ces}")
        json_data = await catalogue_service.index_vc_from_did(
            did=did, publish_to_federated_catalog=publish_to_catalog, publish_to_ces=publish_to_ces
        )

        return fastapi.Response(
            status_code=http.HTTPStatus.CREATED, content=json.dumps(json_data), media_type="application/ld+json"
        )

    @catalogue_router.delete("/catalog/items/", dependencies=[Depends(api_key_auth)], tags=["Indexation"])
    async def delete_item_from_catalog(did: Annotated[str, Body()]) -> Response:
        log.debug(f"DID Received : {did}")
        await catalogue_service.delete_vc_from_did(did=did)
        return fastapi.Response(status_code=http.HTTPStatus.NO_CONTENT)

    @catalogue_router.post("/catalogue/items/", dependencies=[Depends(api_key_auth)], tags=["Indexation"])
    async def post_items_into_catalog(
        dids: Annotated[list, Body()], publish_to_catalogue: bool = True, publish_to_ces: bool = False
    ) -> Response:
        log.debug(f"DIDs Received : {dids}")
        log.debug(f"publish_to_catalog : {publish_to_catalogue}")
        log.debug(f"publish_to_ces : {publish_to_ces}")

        json_data = await catalogue_service.index_vc_from_dids(
            dids=dids, publish_to_federated_catalogue=publish_to_catalogue, publish_to_ces=publish_to_ces
        )

        return fastapi.Response(
            status_code=http.HTTPStatus.CREATED, content=json.dumps(json_data), media_type="application/ld+json"
        )

    @catalogue_router.delete("/catalogue/items/", dependencies=[Depends(api_key_auth)], tags=["Indexation"])
    async def delete_items_from_catalogs(dids: Annotated[list, Body()], publish_to_catalogue: bool = True) -> Response:
        log.debug(f"DIDs Received : {dids}")
        log.debug(f"publish_to_catalog : {publish_to_catalogue}")

        await catalogue_service.delete_vcs_from_dids(dids=dids, publish_to_federated_catalogue=publish_to_catalogue)

        return fastapi.Response(status_code=http.HTTPStatus.NO_CONTENT)

    @catalogue_router.get("/ghost_items", status_code=http.HTTPStatus.OK, tags=["Utilities"])
    async def get_ghost_items() -> set[str]:
        """
        Return item inside catalogue but no more accessible
        """
        return await catalogue_service.get_ghost_items()

    return catalogue_router
