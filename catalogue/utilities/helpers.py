import logging
import urllib.parse
import urllib.request
from functools import reduce
from typing import Optional

log = logging.getLogger(__name__)


def transform_did_into_url(did: Optional[str]) -> Optional[str]:
    """
    The transform_did_into_url function takes a DID (Decentralized Identifier) as input and returns the URL of the
    DID document.

    ### Example Usage
    ```python
    did = "did:web:example.com"
    url = transform_did_into_url(did)
    print(url)
    # Output: "https://example.com"
    ```

    :param did (optional string): A string representing the DID.
    :return url (optional string): The URL of the DID document. If the input DID is empty or None,
    the function returns None.
    """
    log.debug(f"received did: {did}")
    did_url = did
    if did is None or did.lstrip() == "":
        log.debug(f"returns url: {did_url}")
        return None

    if did.startswith("did:web"):
        did = did.replace(":", "/")
        did = did.replace("did/web/", "https://")
        did_url = urllib.parse.unquote_plus(did)

    log.debug(f"returns url: {did_url}")

    return did_url


def get_compliance_file_name(object_type: str) -> str:
    """
    This code defines a function named get_compliance_file_name that takes an object type as input and returns a string
    representing the compliance file name. The function handles different cases for the object type and applies certain
    transformations to generate the file name.

    ### Example Usage
    ```python
    file_name = get_compliance_file_name("user")
    print(file_name)
    # Output: "gaiax-compliance-user.json"
    ```

    :param object_type (string): The type of the object for which the compliance file name is generated.
    :return A string representing the compliance file name based on the given object type.
    """

    match object_type:
        case None | "":
            return ""
        case str():
            if object_type.isupper():
                object_type = object_type.title()

            prefix_index = object_type.find(":")
            object_type = object_type[prefix_index + 1 :]

            to_lower = reduce(lambda x, y: x + ("-" if y.isupper() else "") + y, object_type).lower()
            return f"gaiax-compliance-{to_lower}.json"
        case _:
            return ""
