from abc import ABCMeta, abstractmethod


class MessagePublisher(metaclass=ABCMeta):
    @abstractmethod
    async def publish_catalogue(self, message: dict):
        """
        The publish_catalogue method is an abstract method defined in the MessagePublisher class. It takes a message
        parameter of type dict and is intended to publish a message related to a catalogue. This method must be
        implemented by any class that inherits from MessagePublisher.

        ###Example Usage
        ```python
        class MyMessagePublisher(MessagePublisher):
            async def publish_catalogue(self, message: dict):
                # implementation of the publish_catalogue method

        publisher = MyMessagePublisher()
        message = {"catalog_id": 123, "message": "New catalogue created"}
        await publisher.publish_catalogue(message)
        ```
        :param message (dict): A dictionary containing the details of the catalogue message to be published.
        :return None. The publish_catalogue method is an abstract method and does not return any value.
        """
        raise NotImplementedError

    @abstractmethod
    async def publish_vc(self, message: dict):
        """
        The publish_vc method is an abstract method defined in the MessagePublisher class. It takes a message parameter
        of type dict and is intended to publish a message related to a virtual catalog. This method must be implemented
        by any class that inherits from MessagePublisher.

        ### Example Usage
        ```python
        class MyMessagePublisher(MessagePublisher):
            async def publish_vc(self, message: dict):
                # implementation of the publish_vc method

        publisher = MyMessagePublisher()
        message = {"catalog_id": 123, "message": "New catalog created"}
        await publisher.publish_vc(message)
        ```

        :param message (dict): A dictionary containing the details of the virtual catalog message to be published.
        :return None. The publish_vc method is an abstract method and does not return any value.
        """
        raise NotImplementedError

    @abstractmethod
    async def ping(self) -> bool:
        """
        The ping function is used to check if the server is up and running.
        It returns a boolean value, True if the server responds with a pong message, False otherwise.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value of whether the connection is alive or not
        """
        raise NotImplementedError
