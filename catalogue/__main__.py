import logging
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator

from catalogue.configuration import CATALOG_PREFIX_KEY, Settings, get_settings
from catalogue.domain.interfaces.publisher import MessagePublisher
from catalogue.domain.interfaces.repository import CatalogueRepository
from catalogue.infra.api.rest.api import create_fastapi_application
from catalogue.infra.spi.db.redis import RedisCatalogueRepository
from catalogue.infra.spi.message_queue.ces import CESMessagePublisher
from catalogue.infra.spi.message_queue.redis import RedisMessagePublisher
from catalogue.infra.spi.wallet.participant_agent import ParticipantAgentClient
from catalogue.services.catalogue import CatalogueService
from catalogue.services.healthcheck import HealthcheckService
from catalogue.utilities.observability.logger import configure_logging

if __name__ == "__main__":
    settings: Settings = get_settings()

    configure_logging()

    @asynccontextmanager
    async def lifespan(application: FastAPI):
        """
        The lifespan function is a coroutine that will be executed when the application starts and stops.
        It's used to initialize and finalize things, like connecting to databases or opening network connections.
        The lifespan function receives an instance of FastAPI as parameter, so you can use it for dependency injection.

        Args:
            app: FastAPI: Pass the fastapi object to the lifespan function

        Returns:
            A generator

        """
        log = logging.getLogger(__name__)

        log.info(f"Healthcheck endpoint available on : http://127.0.0.1:{settings.api_port_exposed}/healthcheck")

        instrumentator.expose(application)
        yield

        log.info("Shutting down")

    catalogue_repository: CatalogueRepository = RedisCatalogueRepository(url=str(settings.redis_host))

    publishing_channel = f"{CATALOG_PREFIX_KEY}/{settings.participant_name}"
    catalogue_publisher: MessagePublisher = RedisMessagePublisher(
        channel_name=publishing_channel, url=str(settings.redis_pub_sub_host)
    )
    ces_publisher: MessagePublisher = CESMessagePublisher(url=str(settings.ces_url))

    wallet_client = ParticipantAgentClient()
    catalogue_service = CatalogueService(
        repository=catalogue_repository, wallet_client=wallet_client, publishers=[catalogue_publisher, ces_publisher]
    )
    healthcheck_service = HealthcheckService(repository=catalogue_repository, publisher=catalogue_publisher)

    app = create_fastapi_application(
        catalogue_service=catalogue_service, healthcheck_service=healthcheck_service, lifespan=lifespan
    )
    instrumentator = Instrumentator().instrument(app)
    uvicorn.run(app, host="0.0.0.0", port=settings.api_port_exposed, log_config=None)
