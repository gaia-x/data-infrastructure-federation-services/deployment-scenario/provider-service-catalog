import pytest

from catalogue.utilities.helpers import get_compliance_file_name, transform_did_into_url


@pytest.mark.parametrize(
    "did, expected",
    [
        (None, None),
        ("", None),
        ("did:example:123", "did:example:123"),
        ("did:web:example.com", "https://example.com"),
        ("did:web:example.com%3A5000", "https://example.com:5000"),
        ("did:web:example.com?param1=value1&param2=value2", "https://example.com?param1=value1&param2=value2"),
        (
            "did:web:example.com:service_offering/123456",
            "https://example.com/service_offering/123456",
        ),
        (
            "did:web:example.com:service_offering/123456/data.json",
            "https://example.com/service_offering/123456/data.json",
        ),
        (
            "https://example.com/service_offering/123456/data.json",
            "https://example.com/service_offering/123456/data.json",
        ),
    ],
)
def test_transform_did_into_url(did, expected):
    result = transform_did_into_url(did)

    assert result == expected


class TestGetComplianceFileName:

    #  Test with a valid string input
    def test_valid_string_input(self):
        result = get_compliance_file_name("valid_string")
        assert result == "gaiax-compliance-valid_string.json"

    #  Test with a single letter input
    def test_single_letter_input(self):
        result = get_compliance_file_name("a")
        assert result == "gaiax-compliance-a.json"

    #  Test with a string containing only uppercase letters
    def test_uppercase_letters_input(self):
        result = get_compliance_file_name("UPPERCASE")
        assert result == "gaiax-compliance-uppercase.json"

    #  Test with an empty string input
    def test_empty_string_input(self):
        result = get_compliance_file_name("")
        assert result == ""

    #  Test with a non-string input
    def test_non_string_input(self):
        result = get_compliance_file_name(123)
        assert result == ""

    #  Test with a string containing special characters
    def test_special_characters_input(self):
        result = get_compliance_file_name("!@#$%^&*()")
        assert result == "gaiax-compliance-!@#$%^&*().json"

    #  Test with a string containing only lowercase letters
    def test_lowercase_letters(self):
        result = get_compliance_file_name("lowercase")
        assert result == "gaiax-compliance-lowercase.json"

    #  Test with a string containing both uppercase and lowercase letters
    def test_mixed_case_letters(self):
        result = get_compliance_file_name("MixedCase")
        assert result == "gaiax-compliance-mixed-case.json"

    #  Test with a string containing numbers
    def test_numbers(self):
        result = get_compliance_file_name("12345")
        assert result == "gaiax-compliance-12345.json"

    #  Test with a string containing prefix
    def test_string_with_prefix(self):
        result = get_compliance_file_name("gx:ServiceOffering")
        assert result == "gaiax-compliance-service-offering.json"
