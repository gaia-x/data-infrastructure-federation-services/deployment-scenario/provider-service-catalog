import json
import logging
from typing import Any

import httpx

from catalogue.configuration import Settings, get_settings
from catalogue.domain.catalogue import Catalogue
from catalogue.domain.interfaces.wallet import WalletClient
from catalogue.infra.spi.wallet.dto import catalogue_to_json

log = logging.getLogger(__name__)
settings: Settings = get_settings()

SCHEME = "http://" if "http" not in settings.participant_agent_host else ""
PARTICIPANT_AGENT_VC_REQUEST_URL: str = f"{SCHEME}{settings.participant_agent_host}/api/vc-request"
PARTICIPANT_AGENT_VP_REQUEST_URL: str = f"{SCHEME}{settings.participant_agent_host}/api/vp-request"
PARTICIPANT_AGENT_DEFAULT_HEADER = {
    "x-api-key": settings.participant_agent_secret_key,
    "Content-Type": "application/json",
}


class NotAuthorizedException(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class WrongVCRequestException(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


async def _sign(w3c_object: str, url: str) -> Any:
    """
    The sign function takes a verifiable presentation or a verifiable credential object
    and returns a signed version of that object thanks to provider's participant agent.

    Args:
        self: Represent the instance of the class
        object:str: Specify the object that you want to sign (VP or VC)
        url: endpoint of participant agent

    Returns:
        The sign version of your object
    """
    async with httpx.AsyncClient() as client:
        log.debug(f"Calling {url}")

        response = await client.put(url=url, content=w3c_object, headers=PARTICIPANT_AGENT_DEFAULT_HEADER)

        match response.status_code:
            case httpx.codes.OK:
                return response.json()
            case httpx.codes.BAD_REQUEST:
                raise WrongVCRequestException(str(response.content))
            case httpx.codes.FORBIDDEN:
                raise NotAuthorizedException(response.json()["message"])


class ParticipantAgentClient(WalletClient):
    async def generate_vp_from_catalog(self, catalog: Catalogue, provider_vc: dict[str, Any] | None = None) -> Any:
        """
        The generate_vp_from_catalog function is used to generate a Verifiable Presentation from the given Catalogue.

        Args:
            self: Bind the method to an object
            catalog: Catalogue: Pass in the catalog object
            provider_vc: dict[str: Pass in the did document of the provider
            Any] | None: Specify that the parameter is optional

        Returns:
            A verifiable presentation
        """
        log.debug(f"1- Creating a vc from catalog object {catalog}")
        catalog_vc = await self.generate_vc_from_catalog(catalog=catalog)

        log.debug(f"Catalog VC Generated {catalog_vc}")

        verifiable_credentials = [catalog_vc]

        if provider_vc is not None:
            verifiable_credentials.append(provider_vc)

        return await _sign(w3c_object=json.dumps(verifiable_credentials), url=PARTICIPANT_AGENT_VP_REQUEST_URL)

    async def generate_vc_from_catalog(self, catalog: Catalogue) -> dict:
        """
        The generate_vc_from_catalog function takes in a Catalogue object and returns a Verifiable Credentials object
        signed by provider's participant agent.

        Args:
            self: Represent the instance of the class
            catalog: Catalogue: Get the catalogue object

        Returns:
            A verifiable Credentials

        """
        json_catalog = catalogue_to_json(catalog)
        log.debug(f"catalogue:  {json_catalog}")

        return await _sign(w3c_object=json_catalog, url=PARTICIPANT_AGENT_VC_REQUEST_URL)
