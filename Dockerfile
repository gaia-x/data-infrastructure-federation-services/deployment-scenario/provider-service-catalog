ARG APP_NAME=provider-catalogue
ARG APP_PATH=/opt/$APP_NAME
ARG MODULE_NAME=catalogue
ARG REGISTRY=public.ecr.aws/docker/library
ARG PYTHON_VERSION=3.12.8
ARG POETRY_VERSION=1.7.1
ARG CI_PROJECT_URL
#
# Stage: staging
#
FROM $REGISTRY/python:$PYTHON_VERSION-slim AS staging
ARG APP_NAME
ARG APP_PATH
ARG POETRY_VERSION
ARG MODULE_NAME

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    POETRY_VERSION=${POETRY_VERSION} \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --no-install-recommends -y build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir "poetry==${POETRY_VERSION}"

# Import our project files
WORKDIR ${APP_PATH}
COPY ./poetry.lock ./pyproject.toml ./README.md ./
COPY ./${MODULE_NAME} ./${MODULE_NAME}

#
# Stage: build
#
FROM staging AS build
ARG APP_PATH
ARG APP_NAME

WORKDIR ${APP_PATH}
RUN poetry build --format wheel \
    && poetry export --format requirements.txt --output requirements.txt --without-hashes

#
# Stage: production
#
FROM $REGISTRY/python:$PYTHON_VERSION-alpine AS production
ARG APP_NAME
ARG APP_PATH
ARG CI_PROJECT_URL

LABEL name=$APP_NAME \
    description="A catalogue of self-description objects (ServiceOffering, DataProducts...) dedicated to a provider." \
    url=$CI_PROJECT_URL                 \
    maintainer="OVHcloud"

ENV \
    USER_ID=65535 \
    GROUP_ID=65535 \
    USER_NAME=default-user \
    GROUP_NAME=default-user \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

# hadolint ignore=DL3018
RUN addgroup --gid ${GROUP_ID} ${GROUP_NAME} && \
    adduser --shell /sbin/nologin --disabled-password \
    --no-create-home --uid ${USER_ID} --ingroup ${GROUP_NAME} ${USER_NAME} && \
    apk --no-cache add curl

# Get build artifact wheel and install it respecting dependency versions
WORKDIR ${APP_PATH}
COPY --from=build ${APP_PATH}/dist/*.whl ${APP_PATH}/

COPY --from=build ${APP_PATH}/requirements.txt ${APP_PATH}/


# hadolint ignore=SC2086
RUN pip install --no-cache-dir --upgrade pip==23.3.2 && \
    pip install --no-cache-dir ./*.whl --requirement requirements.txt

EXPOSE ${PORT}
CMD [ "python","-u", "-m", "catalogue" ]
USER ${USER_NAME}
