from typing import Any
from unittest.mock import AsyncMock

import pytest

from catalogue.domain.catalogue import Catalogue
from catalogue.services.catalogue import CatalogueService

VC_CONTENT = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {"gx": "https://w3id.org/gaia-x/gx#", "gx-data": "https://w3id.org/gaia-x/data#"},
    ],
    "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/0987654321/data.json",
    "type": ["VerifiableCredential", "gx-data:DataProduct"],
    "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
    "expirationDate": "2023-07-18T14:45:29.303+02:00",
    "credentialSubject": {
        "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data-product/ba28f8f4a715d8af98d68aa72be0ac5519cd87dfa2345686479ea5/data.json",
        "@type": "gx-data:DataProduct",
        "gx-data:providedBy": "https://docaposte.provider.dev.gaiax.ovh/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
        "gx-data:serviceTermsAndConditions": {
            "gx-data:value": "https://compliance.lab.gaia-x.eu/development",
            "gx-data:hash": "myrandomhash",
        },
        "gx-data:title": "DufourDataTest",
        "gx-data:serviceType": ["Collaboration", "Translation"],
        "gx-data:description": "zfz",
        "gx-data:descriptionMarkDown": "",
        "gx-data:webAddress": "",
        "gx-data:dataProtectionRegime": "",
        "gx-data:dataExport": [
            {"gx-data:requestType": "email", "gx-data:accessType": "digital", "gx-data:formatType": "mime/png"}
        ],
        "gx-data:dependsOn": "",
        "gx-data:keyword": "",
        "gx-data:layer": "SAAS",
        "gx-data:isAvailableOn": [
            "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/80ba30db8dd2622a93be652e8db5b3f3085baabc50d387d1dc3311f76455e22d/data.json"
        ],
        "gx-data:aggregationOf": [
            {
                "gx-data:identifier": "62b32ab3404fc258d3837f89",
                "gx-data:copyrightOwnedBy": "did:web:example.com/api/secure/participant/organisations/62b30fd8404fc258d38369ed",
                "gx-data:license": [],
                "gx-data:personalDataPolicy": "NO_PERSONAL_DATA",
                "gx-data:distribution": [
                    {
                        "gx-data:title": "ice_cream_archived.xls",
                        "gx-data:mediaType": "application/vnd.ms-excel",
                        "gx-data:byteSize": 23552,
                        "gx-data:fileHash": "cea466b7813f9dba5613a7d8e4dbd3915116e8c5441e862224486eb854feeb8a",
                        "gx-data:algorithm": "SHA-256",
                        "gx-data:location": {
                            "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/80ba30db8dd2622a93be652e8db5b3f3085baabc50d387d1dc3311f76455e22d/data.json",
                            "@type": "gx:Location",
                        },
                    }
                ],
            },
            {
                "gx-data:identifier": "62e790b98829212af7dad7ab",
                "gx-data:copyrightOwnedBy": "did:web:example.com/api/secure/participant/organisations/62b30fd8404fc258d38369ed",
                "gx-data:license": [],
                "gx-data:personalDataPolicy": "NO_PERSONAL_DATA",
                "gx-data:distribution": [
                    {
                        "gx-data:title": "multidataset.xls",
                        "gx-data:mediaType": "application/vnd.ms-excel",
                        "gx-data:byteSize": 30965248,
                        "gx-data:fileHash": "44f5f116d381c2ab4dab9459a15ab00e5fb043a66832447f2cc5d6a3a18855f1",
                        "gx-data:algorithm": "SHA-256",
                        "gx-data:location": {
                            "@id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/80ba30db8dd2622a93be652e8db5b3f3085baabc50d387d1dc3311f76455e22d/data.json",
                            "@type": "gx:Location",
                        },
                    }
                ],
            },
        ],
    },
    "issuanceDate": "2023-04-19T12:59:30.960227+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
        "created": "2023-04-19T12:59:30.960227+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_Znb5-xSLsvgA6iL3mymFpK6fh0UsaT9fVrmgGJkTIx4AVlF26rPLPifJxDRS2Rtfpwms4hzfaoFbjKxGWzrLyQpmUkpXs5HB2LdNBHAf24RFyqTB-SbRvpRcsJs1mI3UgLgdv39EruC4p27YajgX6fnMZ5-9n3FkAsAj0YZCMpA1fgmwCeegxLPD_HJ3QFI4pLPSvuH8o8bZjIEiDyfYl1hA77HHLzSx67tA8P8cYlMBgkzrrJQ11MA3t7w6MtB9dpuFueq-Ua4V_uUUieLujTbCb3jOsXQMcXA2og7Zr726IvJs-AqelbtBf5xLIOcc6aqPR0BjSp0TVaVJv_fw",
    },
}


class TestCatalogueService:
    @pytest.mark.anyio
    async def test_get_catalog_should_return_empty_catalog_object(
        self, catalogue_repository: AsyncMock, wallet_client: AsyncMock, empty_catalogue: Catalogue
    ) -> None:
        catalogue_repository.keys.return_value = []

        result = await CatalogueService(repository=catalogue_repository, wallet_client=wallet_client).get()

        assert result == empty_catalogue

    @pytest.mark.anyio
    async def test_get_catalog_should_return_non_empty_catalog_object(
        self, catalogue_repository: AsyncMock, wallet_client: AsyncMock, non_empty_catalogue: Catalogue, vc_did: str
    ) -> None:
        catalogue_repository.keys.return_value = [vc_did]

        result = await CatalogueService(repository=catalogue_repository, wallet_client=wallet_client).get()

        assert result == non_empty_catalogue

    @pytest.mark.anyio
    async def test_delete_catalog_should_return_nothing(
        self, catalogue_repository: AsyncMock, wallet_client: AsyncMock
    ) -> None:
        catalogue_repository.keys.return_value = None

        result = await CatalogueService(repository=catalogue_repository, wallet_client=wallet_client).delete_catalog()

        assert result is None

    @pytest.mark.anyio
    async def test_index_vc(
        self, mocker, catalogue_repository: AsyncMock, wallet_client: AsyncMock, vc_did: str
    ) -> None:
        catalogue_repository.keys.return_value = vc_did

        mocker.patch.object(CatalogueService, attribute="_retrieve_json_content", return_value=VC_CONTENT)
        mocker.patch.object(CatalogueService, attribute="publish_catalog_to_federated_catalog", return_value=None)

        result = await CatalogueService(repository=catalogue_repository, wallet_client=wallet_client).index_vc_from_did(
            did=vc_did
        )

        assert result is VC_CONTENT

    @pytest.mark.anyio
    async def test_index_vc_should_return_none_if_not_exists(
        self, mocker, catalogue_repository: AsyncMock, wallet_client: AsyncMock, vc_did: str
    ) -> None:
        catalogue_repository.keys.return_value = vc_did

        mocker.patch.object(CatalogueService, attribute="_retrieve_json_content", return_value=None)
        mocker.patch.object(CatalogueService, attribute="publish_catalog_to_federated_catalog", return_value=None)

        result = await CatalogueService(repository=catalogue_repository, wallet_client=wallet_client).index_vc_from_did(
            did=vc_did
        )

        assert result is None

    @pytest.mark.anyio
    async def test_delete_vc(
        self, mocker, catalogue_repository: AsyncMock, wallet_client: AsyncMock, vc_did: str
    ) -> None:
        catalogue_repository.delete.return_value = VC_CONTENT
        mocker.patch.object(CatalogueService, attribute="publish_catalog_to_federated_catalog", return_value=None)

        result = await CatalogueService(
            repository=catalogue_repository, wallet_client=wallet_client
        ).delete_vc_from_did(did=vc_did)

        assert result is VC_CONTENT

    @pytest.mark.anyio
    async def test_publish_catalog_to_federated_catalog_should_do_nothing_if_publisher_is_none(
        self, catalogue_repository: AsyncMock, wallet_client: AsyncMock, vc_did: str
    ) -> None:
        result = await CatalogueService(
            repository=catalogue_repository, wallet_client=wallet_client
        ).publish_catalog_to_federated_catalog()

        assert result is False

    @pytest.mark.anyio
    async def test_publish_catalog_to_federated_catalog_should_publish_message_if_publisher_is_not_none_and_catalog_is_empty(
        self,
        catalogue_repository: AsyncMock,
        wallet_client: AsyncMock,
        message_publisher: AsyncMock,
        vp_with_empty_catalogue: Any,
    ) -> None:
        wallet_client.generate_vp_from_catalog.return_value = vp_with_empty_catalogue

        result = await CatalogueService(
            repository=catalogue_repository, wallet_client=wallet_client, publishers=[message_publisher]
        ).publish_catalog_to_federated_catalog()

        assert result is True
