import json
import logging

from redis.asyncio.client import Redis
from redis.exceptions import RedisError

from catalogue.domain.interfaces.publisher import MessagePublisher

log = logging.getLogger(__name__)


class RedisMessagePublisher(MessagePublisher):
    def __init__(self, channel_name: str, url: str = "redis://127.0.0.1:6379/0") -> None:
        """
        The __init__ function is called when the class is instantiated.
        It sets up the connection to Redis and stores it in self.redis_client,
        and also stores the channel name in self.channel_name.

        Args:
            self: Represent the instance of the class
            channel_name: str: Specify the name of the channel that we want to publish messages to
            url: str: Specify the url of the redis server

        Returns:
            None, because it has no return statement
        """
        log.debug(f"Creating publisher to {channel_name} => {url}")
        self.redis_client = Redis.from_url(url, decode_responses=True)
        self.channel_name = channel_name

    async def publish_catalogue(self, message: dict):
        if not isinstance(message, dict):
            raise ValueError("Message must be a dict")

        payload = json.dumps(message)

        try:
            log.debug(f"Publishing message {payload}")
            await self.redis_client.publish(self.channel_name, message=payload)
        except RedisError as e:
            log.error(f"Error publishing message {payload}: {e}")
            raise

    async def ping(self) -> bool:
        """
        The ping function is used to test the connection between the client and server.
        It returns a boolean value of True if it succeeds, or False if it fails.

        Args:
            self: Represent the instance of the class

        Returns:
            A boolean value indicating whether the connection is still alive
        """
        try:
            await self.redis_client.ping()
        except RedisError:
            return False
        return True

    async def publish_vc(self, message: dict):
        return
