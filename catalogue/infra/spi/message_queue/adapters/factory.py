from cloudevents.http import CloudEvent, to_structured

from catalogue.configuration import Settings, get_settings

settings: Settings = get_settings()


def to_cloud_event(payload: dict | str) -> tuple[dict[str, str], bytes]:
    """
    This code defines a function named to_cloud_event that takes a dictionary or string as input and returns a tuple
    containing a dictionary of http headers and a payload as bytes object.
    The function is used to convert the input payload into a CloudEvent structure.

    ### Example Usage
    ```python
    payload = {"key": "value"}
    attributes, event_data = to_cloud_event(payload)
    print(attributes)  # {"type": "eu.gaia-x.credential", "source": "participant_name.parent_domain"}
    print(event_data)  # b'{"key": "value"}'
    ```
    :param payload (dict or str): The payload to be converted into a CloudEvent structure.
    :return attributes (dict[str, str]): A dictionary of attributes for the CloudEvent, including the type and source.
    :return event_data (bytes): The payload data converted into a bytes object.
    """
    attributes = {
        "type": "eu.gaia-x.credential",
        "source": f"{settings.participant_name}.{settings.parent_domain}",
        "datacontenttype": "application/json",
    }

    return to_structured(CloudEvent(attributes, payload))
