from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from catalogue.infra.api.rest.constants import API_DESCRIPTION, API_TITLE, TAGS_METADATA
from catalogue.infra.api.rest.routes import create_common_router
from catalogue.infra.api.rest.routes.catalogue import create_catalogue_router
from catalogue.services.catalogue import CatalogueService
from catalogue.services.healthcheck import HealthcheckService


def create_fastapi_application(
    catalogue_service: CatalogueService, healthcheck_service: HealthcheckService, lifespan=None
) -> FastAPI:
    """
    The create_fastapi_application function creates a FastAPI application object.

    Args:

    Returns:
        A fastapi object
    """
    app = FastAPI(
        lifespan=lifespan,
        docs_url="/api/doc",
        redoc_url=None,
        title=API_TITLE,
        openapi_tags=TAGS_METADATA,
        description=API_DESCRIPTION,
        version="22.10",
        license_info={
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(router=create_common_router(healthcheck_service=healthcheck_service), tags=["Common"])
    app.include_router(router=create_catalogue_router(catalogue_service=catalogue_service))
    return app
