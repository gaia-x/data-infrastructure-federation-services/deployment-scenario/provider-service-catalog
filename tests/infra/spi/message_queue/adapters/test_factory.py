import json

import pytest

from catalogue.infra.spi.message_queue.adapters.factory import to_cloud_event


class TestToCloudEvent:
    def test_it_should_return_tuple(self):
        payload = {}
        result = to_cloud_event(payload)
        assert isinstance(result, tuple)
        assert len(result) == 2
        assert isinstance(result[0], dict)
        assert isinstance(result[1], bytes)

    def test_it_should_return_valid_cloud_event_if_payload_is_none(self):
        payload = None
        headers, body = to_cloud_event(payload)

        cloud_event = json.loads(body.decode("utf-8"))
        assert headers == {"content-type": "application/cloudevents+json"}
        assert "specversion" in cloud_event
        assert "id" in cloud_event
        assert "source" in cloud_event
        assert cloud_event["source"] == "dufourstorage.provider.gaia-x.community"
        assert "type" in cloud_event
        assert cloud_event["type"] == "eu.gaia-x.credential"
        assert "time" in cloud_event
        assert "data" not in cloud_event

    def test_it_should_return_valid_cloud_event_if_payload_is_empty_string(self):
        payload = ""
        headers, body = to_cloud_event(payload)

        cloud_event = json.loads(body.decode("utf-8"))
        assert headers == {"content-type": "application/cloudevents+json"}
        assert "specversion" in cloud_event
        assert "id" in cloud_event
        assert "source" in cloud_event
        assert cloud_event["source"] == "dufourstorage.provider.gaia-x.community"
        assert "type" in cloud_event
        assert cloud_event["type"] == "eu.gaia-x.credential"
        assert "time" in cloud_event
        assert "data" in cloud_event
        assert cloud_event["data"] == ""

    def test_it_should_return_valid_cloud_event_if_payload_is_non_empty_string(self):
        payload = "Test"
        headers, body = to_cloud_event(payload)

        cloud_event = json.loads(body.decode("utf-8"))
        assert headers == {"content-type": "application/cloudevents+json"}
        assert "specversion" in cloud_event
        assert "id" in cloud_event
        assert "source" in cloud_event
        assert cloud_event["source"] == "dufourstorage.provider.gaia-x.community"
        assert "type" in cloud_event
        assert cloud_event["type"] == "eu.gaia-x.credential"
        assert "time" in cloud_event
        assert "data" in cloud_event
        assert cloud_event["data"] == "Test"

    def test_it_should_return_valid_cloud_event_if_payload_is_empty_dict(self):
        payload = {}
        headers, body = to_cloud_event(payload)

        cloud_event = json.loads(body.decode("utf-8"))
        assert headers == {"content-type": "application/cloudevents+json"}
        assert "specversion" in cloud_event
        assert "id" in cloud_event
        assert "source" in cloud_event
        assert cloud_event["source"] == "dufourstorage.provider.gaia-x.community"
        assert "type" in cloud_event
        assert cloud_event["type"] == "eu.gaia-x.credential"
        assert "time" in cloud_event
        assert "data" in cloud_event
        assert cloud_event["data"] == {}

    def test_it_should_return_valid_cloud_event_if_payload_is_empty_dict(self):
        payload = {}
        headers, body = to_cloud_event(payload)

        cloud_event = json.loads(body.decode("utf-8"))
        assert headers == {"content-type": "application/cloudevents+json"}
        assert "specversion" in cloud_event
        assert "id" in cloud_event
        assert "source" in cloud_event
        assert cloud_event["source"] == "dufourstorage.provider.gaia-x.community"
        assert "type" in cloud_event
        assert cloud_event["type"] == "eu.gaia-x.credential"
        assert "time" in cloud_event
        assert "data" in cloud_event
        assert cloud_event["data"] == {}
