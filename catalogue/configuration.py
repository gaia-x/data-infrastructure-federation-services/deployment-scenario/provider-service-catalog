import hashlib
import os
from enum import Enum
from functools import lru_cache
from typing import Final

from dotenv import load_dotenv
from pydantic import Field, field_validator
from pydantic.networks import AnyHttpUrl, RedisDsn
from pydantic_settings import BaseSettings

load_dotenv()

USE_CACHED_SETTINGS = os.getenv("TESTING", "FALSE").lower == "true"

ROOT_DIR: Final = os.path.realpath(os.path.dirname(__file__))

CONTEXT_CATALOG: Final = {
    "@context": ["https://registry.lab.gaia-x.eu/v1/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"],
}

TYPE_CATALOG: Final = {
    "@type": "gx:Catalogue",
}

CATALOG_PREFIX_KEY: Final = "catalogue"


class LogLevelEnum(str, Enum):
    critical = "CRITICAL"
    fatal = "FATAL"
    error = "ERROR"
    warning = "WARNING"
    info = "INFO"
    debug = "DEBUG"


class EnvironmentEnum(str, Enum):
    dev = "DEV"
    prod = "PROD"
    test = "TEST"


class Settings(BaseSettings):
    log_level: LogLevelEnum = LogLevelEnum.debug
    environment: EnvironmentEnum = EnvironmentEnum.dev
    api_key_authorized: str = Field(required=True)
    api_port_exposed: int = Field(default=5001, ge=0, le=65535)
    redis_host: RedisDsn = Field(default="redis://127.0.0.1:6379/0")
    redis_pub_sub_host: RedisDsn = Field(default="redis://127.0.0.1:6379/1")
    ces_url: AnyHttpUrl = Field(default="https://ces-development.lab.gaia-x.eu")
    federation_repository: str = Field(default="abc-federation")
    participant_name: str = Field(default=None, required=True)
    parent_domain: str = Field(default=None, required=True)
    participant_agent_host: str = Field(default=None, required=True)
    participant_agent_secret_key: str = Field(required=True)

    @field_validator("api_key_authorized", "participant_agent_secret_key", mode="before")
    @classmethod
    def apply_root(cls, value: str):
        """
        The apply_root function is a class method that takes in a string value and returns the same string with
        whitespace stripped from both ends of the string. If the input value is None, then it will return None.

        Args:
            cls: Pass the class of the object that is being created
            value: str: Define the type of value that will be passed to the function

        Returns:
            The value of the field if it is none

        """
        value.strip().replace("\n", "")
        return value if value is None else value.strip().replace("\n", "")

    @property
    def participant_hash(self) -> str:
        """
        The participant_hash function is used to generate a unique hash for each participant.
        This hash is used as the key in the dictionary of participants, and it's also stored in
        the database so that we can look up a participant by their name.

        Args:
            self: Represent the instance of the class

        Returns:
            The sha-256 hash of the participant's name

        """
        return hashlib.sha256(bytes(self.participant_name, "utf-8")).hexdigest()

    @property
    def did_url(self) -> str:
        """
        The did_url function returns a string that is the DID URL for this participant.
        The format of the DID URL is:
            did:web:[participant_name].[parent_domain]:participant:[participant_hash]/data.json

        Args:
            self: Bind the method to an object

        Returns:
            The participant's did url
        """
        return (f"https://{self.participant_name}.{self.parent_domain}/"
                f"legal-participant-json/"
                f"{self.participant_hash}"
                f"/data.json")

    @property
    def provider_vc_did_url(self) -> str:
        """
        The provider_vc_did_url function returns a string that is the URL of the DID document for this participant's
        provider VC.
        The URL is in the form:
        did:web:{participant_name}.{parent_domain}:participant:{participant_hash}/vc/provider/data.json
        where {participant_name} and {parent_domain} are replaced with their values, and where {participant_hash} is
        replaced with the SHA-256 hash of this participant's name.

        Args:
            self: Bind the method to an object

        Returns:
            The provider's did url for the vc
        """
        return (
            f"did:web:{self.participant_name}.{self.parent_domain}"
            f":participant:{self.participant_hash}/vc/legal-participant/legal-participant.json"
        )


@lru_cache()
def get_cached_settings() -> Settings:
    """
    bypass pydantic's class instantiation by caching the method
    """
    return Settings()


def get_settings() -> Settings:
    """
    will returned cached settings by default.
    :return:
    """
    return get_cached_settings() if USE_CACHED_SETTINGS else Settings()
